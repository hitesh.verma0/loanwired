/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.18-MariaDB : Database - loanwired
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`loanwired` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `loanwired`;

/*Table structure for table `lw_application_documents` */

DROP TABLE IF EXISTS `lw_application_documents`;

CREATE TABLE `lw_application_documents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `field_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `approve_by` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_application_documents` */

insert  into `lw_application_documents`(`id`,`user_id`,`application_id`,`field_name`,`document_path`,`document`,`status`,`approve_by`,`created_at`,`updated_at`) values 
(1,1,1,'first_salary_slip','personal_loan/PL-10001/','first_salary_slip.pdf',1,0,'2021-04-22 18:43:29','2021-04-22 18:43:29'),
(2,1,1,'second_salary_slip','personal_loan/PL-10001/','second_salary_slip.pdf',1,0,'2021-04-22 18:43:38','2021-04-22 18:43:38'),
(3,1,1,'thired_salary_slip','personal_loan/PL-10001/','thired_salary_slip.pdf',1,0,'2021-04-22 18:43:46','2021-04-22 18:43:46'),
(4,1,1,'three_months_statment','personal_loan/PL-10001/','three_months_statment.pdf',1,0,'2021-04-22 18:44:41','2021-04-22 18:44:41'),
(5,1,1,'electricity_bill','personal_loan/PL-10001/','electricity_bill.pdf',1,0,'2021-04-22 18:44:54','2021-04-22 18:44:54'),
(6,1,1,'other_proof','personal_loan/PL-10001/','other_proof.pdf',1,0,'2021-04-22 18:45:06','2021-04-22 18:45:06'),
(7,1,1,'pan_card','personal_loan/PL-10001/','pan_card.pdf',1,0,'2021-04-22 18:45:14','2021-04-22 18:45:14'),
(8,1,1,'aadhar_card_back','personal_loan/PL-10001/','aadhar_card_back.pdf',1,0,'2021-04-22 18:45:22','2021-04-22 18:45:22'),
(9,1,1,'aadhar_card_front','personal_loan/PL-10001/','aadhar_card_front.pdf',1,0,'2021-04-22 18:45:29','2021-04-22 18:45:29'),
(10,1,1,'applicant_photo','personal_loan/PL-10001/','applicant_photo.png',1,0,'2021-04-22 18:45:46','2021-04-22 18:45:46'),
(11,2,2,'first_salary_slip','busines_loan/BL-10002/','first_salary_slip.pdf',1,0,'2021-04-23 11:27:25','2021-04-23 11:27:25'),
(12,2,2,'second_salary_slip','busines_loan/BL-10002/','second_salary_slip.pdf',1,0,'2021-04-23 11:30:54','2021-04-23 11:30:54'),
(13,2,2,'thired_salary_slip','busines_loan/BL-10002/','thired_salary_slip.pdf',1,0,'2021-04-23 11:33:56','2021-04-23 11:33:56'),
(14,2,2,'three_months_statment','busines_loan/BL-10002/','three_months_statment.pdf',1,0,'2021-04-23 11:41:26','2021-04-23 11:41:26'),
(15,2,2,'aadhar_card_front','busines_loan/BL-10002/','aadhar_card_front.png',1,0,'2021-04-23 12:14:43','2021-04-23 12:14:43'),
(16,2,2,'electricity_bill','busines_loan/BL-10002/','electricity_bill.png',1,0,'2021-04-23 12:14:54','2021-04-23 12:14:54'),
(17,2,2,'other_proof','busines_loan/BL-10002/','other_proof.png',1,0,'2021-04-23 12:15:02','2021-04-23 12:15:02'),
(18,2,2,'aadhar_card_back','busines_loan/BL-10002/','aadhar_card_back.pdf',0,0,'2021-04-23 12:15:09','2021-04-23 12:15:09'),
(19,2,2,'pan_card','busines_loan/BL-10002/','pan_card.png',1,0,'2021-04-23 12:54:14','2021-04-23 12:54:14'),
(20,3,6,'first_salary_slip','busines_loan/BL-10006/','first_salary_slip.png',1,0,'2021-04-23 17:23:28','2021-04-23 17:23:28'),
(21,3,6,'second_salary_slip','busines_loan/BL-10006/','second_salary_slip.png',1,0,'2021-04-23 17:28:07','2021-04-23 17:28:07'),
(22,3,6,'thired_salary_slip','busines_loan/BL-10006/','thired_salary_slip.pdf',1,0,'2021-04-23 17:31:15','2021-04-23 17:31:15'),
(23,3,6,'three_months_statment','busines_loan/BL-10006/','three_months_statment.pdf',1,0,'2021-04-23 17:39:12','2021-04-23 17:39:12'),
(24,3,6,'first_month_statment','busines_loan/BL-10006/','first_month_statment.pdf',1,0,'2021-04-24 10:50:34','2021-04-24 10:50:34'),
(25,3,6,'second_month_statment','busines_loan/BL-10006/','second_month_statment.pdf',1,0,'2021-04-24 11:00:32','2021-04-24 11:00:32'),
(26,3,6,'aadhar_card_front','busines_loan/BL-10006/','aadhar_card_front.png',1,0,'2021-04-24 11:35:58','2021-04-24 11:35:58'),
(27,3,6,'thired_month_statment','busines_loan/BL-10006/','thired_month_statment.pdf',1,0,'2021-04-24 16:20:47','2021-04-24 16:20:47'),
(28,2,2,'applicant_photo','busines_loan/BL-10002/','applicant_photo.png',1,0,'2021-04-25 09:03:03','2021-04-25 09:03:03'),
(29,4,8,'first_salary_slip','personal_loan/PL-10008/','first_salary_slip.png',1,0,'2021-04-27 08:55:04','2021-04-27 08:55:04'),
(30,5,9,'first_salary_slip','personal_loan/PL-10009/','first_salary_slip.png',1,0,'2021-04-28 11:06:24','2021-04-28 11:06:24'),
(31,5,9,'second_salary_slip','personal_loan/PL-10009/','second_salary_slip.pdf',1,0,'2021-04-28 11:06:32','2021-04-28 11:06:32'),
(32,5,9,'thired_salary_slip','personal_loan/PL-10009/','thired_salary_slip.pdf',1,0,'2021-04-28 11:06:40','2021-04-28 11:06:40'),
(33,5,9,'three_months_statment','personal_loan/PL-10009/','three_months_statment.pdf',1,0,'2021-04-30 17:07:03','2021-04-30 17:07:03'),
(34,5,9,'first_month_statment','personal_loan/PL-10009/','first_month_statment.pdf',1,0,'2021-04-30 17:35:41','2021-04-30 17:35:41'),
(35,5,9,'applicant_photo','personal_loan/PL-10009/','applicant_photo.png',1,0,'2021-04-30 17:51:23','2021-04-30 17:51:23');

/*Table structure for table `lw_application_erequests` */

DROP TABLE IF EXISTS `lw_application_erequests`;

CREATE TABLE `lw_application_erequests` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `api` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_limit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `response_status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_application_erequests` */

insert  into `lw_application_erequests`(`id`,`user_id`,`application_id`,`api`,`session_id`,`approved_limit`,`request`,`response`,`response_status`,`created_at`,`updated_at`) values 
(1,5,9,'uat_login_user','8qmi20f7w0ap87tsf1tktecsgd00l67v',NULL,'{\"phone\":\"+919000003383\"}','{\"session-token\":\"8qmi20f7w0ap87tsf1tktecsgd00l67v\"}','success','2021-04-30 17:30:34','2021-04-30 17:30:34'),
(2,5,9,'check_eligibility','8qmi20f7w0ap87tsf1tktecsgd00l67v',NULL,'{\"pan\":\"ABFPB1509L\",\"first_name\":\"Hitesh\",\"last_name\":\"Verma\",\"date_of_birth\":\"1942-04-02\",\"employment_type\":\"salaried\",\"gender\":\"Male\",\"monthly_income\":\"50000\",\"postal_code\":\"400604\"}','{\"is_declined\":false,\"approved_limit\":100000,\"details_required\":[{\"type\":\"income_verification\",\"url\":\"https:\\/\\/api.paysense.io\\/v1\\/external\\/perfios\\/pre_start?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtaWQiOjEwMDc5MjM3LCJ2ZW5kb3IiOnRydWUsImV4cGlyZV90cyI6MTYxOTg5MDIzNi44NzEwNzQsInBhcnRuZXJfc2VjcmV0X2tleSI6InR1QVpTNXZVU0ZHN3VCaGpRYzlMdHVfOGJsaVpIVG50In0.CuvNc-b_cDfOOZduXt7i9xJ3_NvOtQAG156mu3sKTRg\"}],\"application\":null}','success','2021-04-30 17:30:35','2021-04-30 17:30:35');

/*Table structure for table `lw_applications` */

DROP TABLE IF EXISTS `lw_applications`;

CREATE TABLE `lw_applications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employment_type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT 1 COMMENT 'Loan Type',
  `bank_id` int(11) DEFAULT NULL,
  `loan_file_num` int(11) DEFAULT NULL,
  `salary_type` tinyint(4) NOT NULL DEFAULT 1,
  `property_type` tinyint(4) NOT NULL DEFAULT 1,
  `step_completed` tinyint(4) NOT NULL DEFAULT 1,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary_monthly` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `professional_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loan_amount` double(8,2) DEFAULT NULL,
  `account_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `netbanking_uname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `netbanking_pass` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_monthly_statment` tinyint(4) NOT NULL DEFAULT 1,
  `bank_statment_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=>Bank statement,2=>Net banking',
  `next_step` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 2,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_applications` */

insert  into `lw_applications`(`id`,`user_id`,`employment_type_id`,`product_id`,`bank_id`,`loan_file_num`,`salary_type`,`property_type`,`step_completed`,`company_name`,`salary_monthly`,`professional_email`,`loan_amount`,`account_number`,`ifsc_code`,`netbanking_uname`,`netbanking_pass`,`has_monthly_statment`,`bank_statment_type`,`next_step`,`remember_token`,`status`,`created_at`,`updated_at`) values 
(1,1,1,1,3,10001,2,1,5,'test','300','hitesh.verma0@gmail.com',NULL,'4444445454','4545455',NULL,NULL,1,1,'thank-you',NULL,0,'2021-04-22 18:32:40','2021-04-22 18:32:40'),
(2,2,1,2,27,10002,1,1,2,'test','5000000','hitesh@netleon.com',NULL,'231231231231231','IBKL0000058',NULL,NULL,1,1,'congratulations',NULL,2,'2021-04-23 11:19:54','2021-04-23 11:19:54'),
(6,3,1,2,1,10006,2,1,4,'Department of it','20000','hitesh@gmail.com',NULL,'121312312332234234','IBKL0000055',NULL,NULL,2,1,'kyc-documents',NULL,2,'2021-04-23 16:39:07','2021-04-23 16:39:07'),
(8,4,1,1,NULL,10008,1,1,2,'test','50000','hitesh+quiz230320212332@gmail.com',NULL,NULL,NULL,NULL,NULL,1,1,'congratulations',NULL,2,'2021-04-27 08:41:59','2021-04-27 08:41:59'),
(9,5,1,1,6,10009,1,1,4,'test','50000','hitesh+quiz18022021220256@g.com',NULL,'41111111111111111','IBKL0000055',NULL,NULL,1,1,'kyc-documents',NULL,2,'2021-04-28 11:06:07','2021-04-28 11:06:07');

/*Table structure for table `lw_areas` */

DROP TABLE IF EXISTS `lw_areas`;

CREATE TABLE `lw_areas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_areas` */

/*Table structure for table `lw_banks` */

DROP TABLE IF EXISTS `lw_banks`;

CREATE TABLE `lw_banks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_banks` */

insert  into `lw_banks`(`id`,`name`,`flag`,`flag_path`,`status`,`created_at`,`updated_at`) values 
(1,'ICICI',NULL,NULL,0,NULL,NULL),
(2,'SBI',NULL,NULL,0,NULL,NULL),
(3,'KOTAK',NULL,NULL,0,NULL,NULL),
(4,'YES',NULL,NULL,0,NULL,NULL),
(5,'YES_CORP',NULL,NULL,0,NULL,NULL),
(6,'HDFC',NULL,NULL,0,NULL,NULL),
(7,'AXIS',NULL,NULL,0,NULL,NULL),
(8,'SYNDICATE',NULL,NULL,0,NULL,NULL),
(9,'CORPORATION',NULL,NULL,0,NULL,NULL),
(10,'PNB',NULL,NULL,0,NULL,NULL),
(11,'UNITED_BANK',NULL,NULL,0,NULL,NULL),
(12,'INDUSIND',NULL,NULL,0,NULL,NULL),
(13,'CENTRAL_BANK',NULL,NULL,0,NULL,NULL),
(14,'CITI',NULL,NULL,0,NULL,NULL),
(15,'DBS',NULL,NULL,0,NULL,NULL),
(16,'CANARA',NULL,NULL,0,NULL,NULL),
(17,'STANDARD_CHARTERED',NULL,NULL,0,NULL,NULL),
(18,'RBL',NULL,NULL,0,NULL,NULL),
(19,'UNION_BANK',NULL,NULL,0,NULL,NULL),
(20,'ALLAHABAD',NULL,NULL,0,NULL,NULL),
(21,'BANK_OF_BARODA',NULL,NULL,0,NULL,NULL),
(22,'STATE_BANK_PATIALA',NULL,NULL,0,NULL,NULL),
(23,'STATE_BANK_MYSORE',NULL,NULL,0,NULL,NULL),
(24,'STATE_BANK_TRAVANCORE',NULL,NULL,0,NULL,NULL),
(25,'TAMILNAD_MERCANTILE',NULL,NULL,0,NULL,NULL),
(26,'STATE_BANK_BIKANER_JAIPUR',NULL,NULL,0,NULL,NULL),
(27,'STATE_BANK_HYDERABAD',NULL,NULL,0,NULL,NULL),
(28,'BANK_OF_INDIA',NULL,NULL,0,NULL,NULL),
(29,'BANK_OF_MAHARASHTRA',NULL,NULL,0,NULL,NULL),
(30,'BARODA_RAJASTHAN_KSHETRIYA_GRAMIN_BANK',NULL,NULL,0,NULL,NULL),
(31,'DENA_BANK',NULL,NULL,0,NULL,NULL),
(32,'INDIAN_BANK',NULL,NULL,0,NULL,NULL),
(33,'INDIAN_OVERSEAS',NULL,NULL,0,NULL,NULL),
(34,'ORIENTAL_BANK',NULL,NULL,0,NULL,NULL),
(35,'PUNJAB_SIND',NULL,NULL,0,NULL,NULL),
(36,'UCO',NULL,NULL,0,NULL,NULL),
(37,'VIJAYA',NULL,NULL,0,NULL,NULL),
(38,'IDBI',NULL,NULL,0,NULL,NULL),
(39,'BANDHAN',NULL,NULL,0,NULL,NULL),
(40,'CATHOLIC_SYRIAN',NULL,NULL,0,NULL,NULL),
(41,'CITY_UNION',NULL,NULL,0,NULL,NULL),
(42,'DHANLAXMI',NULL,NULL,0,NULL,NULL),
(43,'DCB',NULL,NULL,0,NULL,NULL),
(44,'FEDERAL',NULL,NULL,0,NULL,NULL),
(45,'IDFC',NULL,NULL,0,NULL,NULL),
(46,'KARNATAKA',NULL,NULL,0,NULL,NULL),
(47,'JAMMU_KASHMIR',NULL,NULL,0,NULL,NULL),
(48,'KARUR_VYASA',NULL,NULL,0,NULL,NULL),
(49,'LAKSHMI_VILAS',NULL,NULL,0,NULL,NULL),
(50,'NAINITAL',NULL,NULL,0,NULL,NULL),
(51,'SOUTH_INDIAN',NULL,NULL,0,NULL,NULL),
(52,'RBS',NULL,NULL,0,NULL,NULL),
(53,'SARASWAT_BANK',NULL,NULL,0,NULL,NULL),
(54,'TJSB',NULL,NULL,0,NULL,NULL),
(55,'DNB',NULL,NULL,0,NULL,NULL),
(56,'DNS',NULL,NULL,0,NULL,NULL),
(57,'DEUTSCHE_BANK',NULL,NULL,0,NULL,NULL),
(58,'GREATER_BOMBAY',NULL,NULL,0,NULL,NULL),
(59,'APNA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(60,'AKHAND_ANAND',NULL,NULL,0,NULL,NULL),
(61,'ADARSH_CO_OPERATIVE_URBAN_BANK',NULL,NULL,0,NULL,NULL),
(62,'BASSEIN_CATHOLIC',NULL,NULL,0,NULL,NULL),
(63,'ANDHRA_BANK',NULL,NULL,0,NULL,NULL),
(64,'AMBERNATH_JAI_HIND_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(65,'PUNJAB_MAHARASHTRA_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(66,'HSBC',NULL,NULL,0,NULL,NULL),
(67,'BHARAT',NULL,NULL,0,NULL,NULL),
(68,'SVC_BANK',NULL,NULL,0,NULL,NULL),
(69,'SDC',NULL,NULL,0,NULL,NULL),
(70,'SREENIDHI_SOUHARDA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(71,'OCEAN_FIRST',NULL,NULL,0,NULL,NULL),
(72,'CAPITAL_SMALL_FINANCE_BANK_LTD',NULL,NULL,0,NULL,NULL),
(73,'KALUPUR_COMMERCIAL_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(74,'KANGRA_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(75,'BHARTIYA_MAHILA_BANK',NULL,NULL,0,NULL,NULL),
(76,'SARDAR_BHILADWALA_PARDI_PEOPLES_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(77,'MODEL_CO_OPERATIVE_BANK',NULL,NULL,0,NULL,NULL),
(78,'MEHSANA_URBAN_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(79,'NEW_INDIA_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(80,'THE_NARODA_NAGRIK_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(81,'COSMOS_BANK',NULL,NULL,0,NULL,NULL),
(82,'JANAKALYAN_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(83,'JANATA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(84,'MJALGAON_JANATA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(85,'MANVI_PATTANA_SOUHARADA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(86,'CENTRAL_CO_OPERATIVE_BANK',NULL,NULL,0,NULL,NULL),
(87,'NUTAN_NAGARIK_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(88,'KALYAN_JANATA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(89,'ABHYUDAYA_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(90,'AIRTEL_BANK',NULL,NULL,0,NULL,NULL),
(91,'EQUITAS_SMALL_FINANCE_BANK',NULL,NULL,0,NULL,NULL),
(92,'AU_SMALL_FINANCE_BANK',NULL,NULL,0,NULL,NULL),
(93,'NAGPUR_NAGARIK_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(94,'TEXTILE_CO_OPERATIVE_BANK',NULL,NULL,0,NULL,NULL),
(95,'JANASEVA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(96,'SARASPUR_NAGARIK_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(97,'GP_PARSIK_BANK',NULL,NULL,0,NULL,NULL),
(98,'TGMC_BANK',NULL,NULL,0,NULL,NULL),
(99,'SARVODAYA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(100,'SHREE_KADI_NAGARIK_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(101,'SAURASHTRA_GRAMIN_BANK',NULL,NULL,0,NULL,NULL),
(102,'ASSOCIATE_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(103,'SOLAPUR_JANATA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(104,'INDRAPRASTHA_SEHKARI_BANK',NULL,NULL,0,NULL,NULL),
(105,'MAHESH_SAHAKARI_BANK_LTD',NULL,NULL,0,NULL,NULL),
(106,'PUNE_PEOPLES_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(107,'VASAI_VIKAS_SAH_BANK_LTD',NULL,NULL,0,NULL,NULL),
(108,'MAHESH_BANK',NULL,NULL,0,NULL,NULL),
(109,'UNION_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(110,'JANATHA_SEVA_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(111,'The_Wai_Urban_Co_Operative_Bank_Limited',NULL,NULL,0,NULL,NULL),
(112,'The_Vallabh_VidyaNagar_Commercial_Co_Operative_Bank_Ltd',NULL,NULL,0,NULL,NULL),
(113,'THE_VIJAY_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(114,'ZOROASTRIAN_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(115,'VAIJAPUR_MARCHANTS_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(116,'NKGSB_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(117,'THE_SUTEX_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(118,'THE_SURAT_PEOPLE_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(119,'GUJARAT_AMBUJA_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(120,'KANKARIAA_MANINAGAR_NAG_SAH_BANK_LTD',NULL,NULL,0,NULL,NULL),
(121,'UJJIVAN_SMALL_FINANCE_BANK',NULL,NULL,0,NULL,NULL),
(122,'CITIZEN_CREDIT_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(123,'PADMAVATHI_CO_OP_URBAN_BANK',NULL,NULL,0,NULL,NULL),
(124,'THE_SATARA_SAHAKARI_BANK_LTD',NULL,NULL,0,NULL,NULL),
(125,'MAHANAGAR_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(126,'SURYODAY_SMALL_FINANCE_BANK_LTD',NULL,NULL,0,NULL,NULL),
(127,'SHREE_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(128,'MANINAGAR_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(129,'THE_VARACHHA_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(130,'SURAT_NATIONAL_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(131,'VIDYA_SAHKARI_BANK_LTD',NULL,NULL,0,NULL,NULL),
(132,'PRERANA_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(133,'THE_KARAD_URBAN_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(134,'THE_PANCHSHEEL_MERCANTILE_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(135,'MAGAVEERA_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(136,'THANE_BHARAT_SAHAKARI_BANK_LTD',NULL,NULL,0,NULL,NULL),
(137,'SREE_MAHAYOGI_LAKSHMAMMA_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(138,'VIDARBHA_MERCHANTS_URBAN_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(139,'PRATHAMA_BANK',NULL,NULL,0,NULL,NULL),
(140,'PRIME_BANK',NULL,NULL,0,NULL,NULL),
(141,'DR_BABASAHEB_AMBEDKAR_MULTISTATE_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(142,'The_Hindusthan_Co_Op_Bank_Ltd',NULL,NULL,0,NULL,NULL),
(143,'KARNAVATI_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(144,'The_Ahmednagar_Merchant_Co_Op_Bank_Ltd',NULL,NULL,0,NULL,NULL),
(145,'THE_KANAKAMAHALAKSHMI_CO_OP_BANK_LTD',NULL,NULL,0,NULL,NULL),
(146,'KURMANCHAL_BANK',NULL,NULL,0,NULL,NULL),
(147,'The_Gandhidham_CoOperative_Bank_Ltd',NULL,NULL,0,NULL,NULL),
(148,'SHRI_ARIHANT_COOP_BANK',NULL,NULL,0,NULL,NULL),
(149,'Kallappanna_Awade_Bank',NULL,NULL,0,NULL,NULL),
(150,'Shivalik_Mercantile_Bank',NULL,NULL,0,NULL,NULL),
(151,'SAHYDRI_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(152,'Bihar_Gramin_Bank',NULL,NULL,0,NULL,NULL),
(153,'ANNASAHAB_MAGAR_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(154,'The_Vishweshwar_Sahakari_Bank_Ltd',NULL,NULL,0,NULL,NULL),
(155,'PURVANCHAL_BANK',NULL,NULL,0,NULL,NULL),
(156,'THE_RAJKOT_COMMERCIAL_COOP_BANK',NULL,NULL,0,NULL,NULL),
(157,'The_Jalgaon_Peoples_CoOp_Bank',NULL,NULL,0,NULL,NULL),
(158,'AHMEDABAD_DISTRICT_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(159,'SHIVALIK_MERCANTILE_BANK',NULL,NULL,0,NULL,NULL),
(160,'The_Akola_District_Central_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(161,'The_Panipat_Urban_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(162,'SHRI_RAJKOT_DISTRICT_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(163,'NEELKANT_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(164,'The_Eenadu_Co_Op_Urban_Bank',NULL,NULL,0,NULL,NULL),
(165,'The_Ahmedabad_Mercantile_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(166,'Adarsh_Co_Operative_Bank_Ltd',NULL,NULL,0,NULL,NULL),
(167,'Karnataka_Vikas_Grameena_Bank',NULL,NULL,0,NULL,NULL),
(168,'Noida_Commercial_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(169,'Jai_Tuljabhavani_Urban_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(170,'The_Bardoli_Nagarik_Sahakari_Bank',NULL,NULL,0,NULL,NULL),
(171,'RMGB',NULL,NULL,0,NULL,NULL),
(172,'Sterling_Urban_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(173,'Smriti_Nagrik_Sahakari_Bank',NULL,NULL,0,NULL,NULL),
(174,'Fino_Payments_Bank',NULL,NULL,0,NULL,NULL),
(175,'Vaishya_Nagari_Sahakari_Bank',NULL,NULL,0,NULL,NULL),
(176,'Vananchal_Gramin_Bank',NULL,NULL,0,NULL,NULL),
(177,'Utkarsh_Small_Finance_Bank',NULL,NULL,0,NULL,NULL),
(178,'Maharashtra_Gramin_Bank',NULL,NULL,0,NULL,NULL),
(179,'Uttarakhand_Gramin_Bank',NULL,NULL,0,NULL,NULL),
(180,'Bhagyodaya_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(181,'BANK_OF_AMERICA',NULL,NULL,0,NULL,NULL),
(182,'Manorama_Co_op_Bank_Solapur',NULL,NULL,0,NULL,NULL),
(183,'Shree_Mahesh_Co_op_Bank_Nashik',NULL,NULL,0,NULL,NULL),
(184,'SANGOLA_URBAN_CO_OPERATIVE_BANK_LTD',NULL,NULL,0,NULL,NULL),
(185,'Gujarat_State_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(186,'Dahod_Urban_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(187,'Sanmathi_Sahakari_Bank',NULL,NULL,0,NULL,NULL),
(188,'Jana_Small_Finance_Bank',NULL,NULL,0,NULL,NULL),
(189,'ADINATH_CO_OP_BANK',NULL,NULL,0,NULL,NULL),
(190,'Deccan_Merchants_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(191,'Seva_Vikas_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(192,'Vikas_Sahakari_Bank',NULL,NULL,0,NULL,NULL),
(193,'SAMATA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(194,'Jamia_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(195,'Pune_Urban_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(196,'The_Sind_Co_Op_Urban_Bank',NULL,NULL,0,NULL,NULL),
(197,'Himachal_Pradesh_State_Co_Op_Bank',NULL,NULL,0,NULL,NULL),
(198,'KEB_HANA_BANK',NULL,NULL,0,NULL,NULL),
(199,'SUCO_SOUHARDA_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(200,'ARVIND_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(201,'RAJARSHI_SHAHU_SAHAKARI_BANK',NULL,NULL,0,NULL,NULL),
(202,'Nidhi_Co_Op_Bank',NULL,NULL,0,NULL,NULL);

/*Table structure for table `lw_branches` */

DROP TABLE IF EXISTS `lw_branches`;

CREATE TABLE `lw_branches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_branches` */

/*Table structure for table `lw_designations` */

DROP TABLE IF EXISTS `lw_designations`;

CREATE TABLE `lw_designations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_designations` */

/*Table structure for table `lw_document_products` */

DROP TABLE IF EXISTS `lw_document_products`;

CREATE TABLE `lw_document_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL COMMENT 'FK',
  `document_id` int(11) NOT NULL COMMENT 'FK ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_document_products` */

/*Table structure for table `lw_documents` */

DROP TABLE IF EXISTS `lw_documents`;

CREATE TABLE `lw_documents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_categories` int(11) NOT NULL DEFAULT 1,
  `ext_allow` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_num` tinyint(4) NOT NULL DEFAULT 1,
  `is_required` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_documents` */

/*Table structure for table `lw_employment_types` */

DROP TABLE IF EXISTS `lw_employment_types`;

CREATE TABLE `lw_employment_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_employment_types` */

insert  into `lw_employment_types`(`id`,`name`,`status`,`created_at`,`updated_at`) values 
(1,'salaried',1,NULL,NULL);

/*Table structure for table `lw_failed_jobs` */

DROP TABLE IF EXISTS `lw_failed_jobs`;

CREATE TABLE `lw_failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `lw_failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_failed_jobs` */

/*Table structure for table `lw_migrations` */

DROP TABLE IF EXISTS `lw_migrations`;

CREATE TABLE `lw_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_migrations` */

insert  into `lw_migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2021_04_06_210327_create_roles_table',1),
(5,'2021_04_06_214112_create_staff_users_table',1),
(6,'2021_04_06_214810_create_designations_table',1),
(7,'2021_04_06_215546_create_zones_table',1),
(8,'2021_04_06_215625_create_areas_table',1),
(9,'2021_04_06_215644_create_branches_table',1),
(10,'2021_04_12_105655_create_products_table',1),
(11,'2021_04_13_111418_create_quick_applications_table',1),
(12,'2021_04_14_132212_create_sms_logs_table',1),
(13,'2021_04_14_145612_create_applications_table',1),
(14,'2021_04_14_151332_create_documents_table',1),
(15,'2021_04_14_151349_create_application_documents_table',1),
(16,'2021_04_14_154106_create_document_products_table',1),
(17,'2021_04_14_185618_create_banks_table',1),
(18,'2021_04_14_185832_create_employment_types_table',1),
(19,'2021_04_26_085841_create_application_uatrequests_table',2);

/*Table structure for table `lw_password_resets` */

DROP TABLE IF EXISTS `lw_password_resets`;

CREATE TABLE `lw_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `lw_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_password_resets` */

/*Table structure for table `lw_products` */

DROP TABLE IF EXISTS `lw_products`;

CREATE TABLE `lw_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `loan_file_prefix` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `age_to_apply` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `interest` double(8,2) NOT NULL DEFAULT 0.10,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lw_products_slug_unique` (`slug`),
  UNIQUE KEY `lw_products_loan_file_prefix_unique` (`loan_file_prefix`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_products` */

insert  into `lw_products`(`id`,`name`,`slug`,`loan_file_prefix`,`description`,`amount`,`age_to_apply`,`interest`,`image`,`image_path`,`status`,`created_at`,`updated_at`) values 
(1,'Personal Loan','personal-loan','PL-','',50000.00,'65',0.10,NULL,NULL,1,NULL,NULL),
(2,'Busines Loan','busines-loan','BL-','',50000.00,'65',0.10,NULL,NULL,1,NULL,NULL);

/*Table structure for table `lw_quick_applications` */

DROP TABLE IF EXISTS `lw_quick_applications`;

CREATE TABLE `lw_quick_applications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `otp` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent_otp_number` tinyint(4) DEFAULT NULL,
  `has_mobile_verified` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lw_quick_applications_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_quick_applications` */

insert  into `lw_quick_applications`(`id`,`mobile`,`otp`,`sent_otp_number`,`has_mobile_verified`,`created_at`,`updated_at`) values 
(3,'2342342342','3000',1,0,'2021-04-23 13:33:11','2021-04-23 13:33:11'),
(7,'8118812835','6878',4,0,'2021-04-28 10:35:26','2021-04-28 11:03:51'),
(8,'7737595951','7604',1,0,'2021-04-30 16:58:54','2021-04-30 16:58:54');

/*Table structure for table `lw_roles` */

DROP TABLE IF EXISTS `lw_roles`;

CREATE TABLE `lw_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_roles` */

/*Table structure for table `lw_sms_logs` */

DROP TABLE IF EXISTS `lw_sms_logs`;

CREATE TABLE `lw_sms_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_sms_logs` */

/*Table structure for table `lw_staff_users` */

DROP TABLE IF EXISTS `lw_staff_users`;

CREATE TABLE `lw_staff_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT 4,
  `emp_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=>Male,2=>Female,3=>Transgender',
  `dob` date NOT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `present_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doj` date NOT NULL,
  `designation_id` int(11) NOT NULL,
  `reporting_manager` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zone_id` tinyint(4) NOT NULL,
  `branch_id` tinyint(4) NOT NULL,
  `area_id` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lw_staff_users_email_unique` (`email`),
  UNIQUE KEY `lw_staff_users_mobile_unique` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_staff_users` */

/*Table structure for table `lw_users` */

DROP TABLE IF EXISTS `lw_users`;

CREATE TABLE `lw_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT 4,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `midname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=>Male,2=>Female,3=>Transgender',
  `dob` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otp` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent_otp_number` tinyint(4) DEFAULT NULL,
  `has_mobile_verified` tinyint(4) NOT NULL DEFAULT 0,
  `aadhar_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pan_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `register_from` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1=>Fronted,2=>Admin-panel',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lw_users_mobile_unique` (`mobile`),
  UNIQUE KEY `lw_users_email_unique` (`email`),
  UNIQUE KEY `lw_users_aadhar_num_unique` (`aadhar_num`),
  UNIQUE KEY `lw_users_pan_num_unique` (`pan_num`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_users` */

insert  into `lw_users`(`id`,`role_id`,`name`,`fname`,`midname`,`lname`,`mobile`,`email`,`gender`,`dob`,`age`,`password`,`otp`,`sent_otp_number`,`has_mobile_verified`,`aadhar_num`,`pan_num`,`pincode`,`profile`,`image_path`,`status`,`register_from`,`email_verified_at`,`remember_token`,`created_at`,`updated_at`) values 
(1,4,'hitesh verma','hitesh',NULL,'verma','9000003381','hitesh.verma0@gmail.com',1,'1996-04-05',NULL,NULL,NULL,NULL,1,'41111111111111','AXFPB1509J','302010',NULL,NULL,1,1,NULL,'LjbCdd1udltWER6T3r1QbkP52eilUXwynfSA2Nj9JLa6y95P6TE53mUeZomx','2021-04-22 18:09:06','2021-04-22 18:09:06'),
(2,4,'hitesh verma','hitesh',NULL,'verma','9000003379','hitesh4g44@gmail.com',1,'1992-02-07',29,NULL,NULL,NULL,1,'444444444444','AXFPB1509L\r\n\r\n\r\n\r\n','400008',NULL,NULL,1,1,NULL,'9IPAEFaNQj4NcJm4CRqv5VIDKNboTMg4VlLVeny0U6IUUYaU4FBcOgHZTNMk','2021-04-23 11:02:20','2021-04-26 07:16:58'),
(3,4,'Hitesh verma','Hitesh',NULL,'verma','2132323424','hitesh@netledon.com',1,'1990-04-04',NULL,NULL,NULL,NULL,1,'411111111111','AXFPB1509Y','30201',NULL,NULL,1,1,NULL,'zokqIprPIAmsxYND0SR2LSLbhfYZgL2wkCv3fWrxfa74F9cWkz7quh6gFXMy','2021-04-23 14:24:13','2021-04-24 04:26:48'),
(4,4,'hitesh verma','hitesh',NULL,'verma','1236666666','hitesh@gmail.com',2,'1998-04-01',23,NULL,NULL,NULL,1,'411111166666','AAAAA9999D','302010',NULL,NULL,1,1,NULL,'HuqGa93TSJCprway4RuSx3YoknQ3KeHX8VCETids8exFvDNlY7M0aDNxD2hT','2021-04-27 08:41:22','2021-04-27 08:41:22'),
(5,4,'hitesh verma','hitesh',NULL,'verma','9000003383','hitesh1237@gmail.com',1,'1942-04-02',79,NULL,NULL,NULL,1,'411111144444','ABFPB1509L','400604',NULL,NULL,1,1,NULL,'jxOsHumVfp10bEAI5eZAZKnALpfA05nhn2qOVuf3Jjy2LQjC6yP8Thmbz2AR','2021-04-27 12:01:04','2021-04-30 16:59:27');

/*Table structure for table `lw_zones` */

DROP TABLE IF EXISTS `lw_zones`;

CREATE TABLE `lw_zones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lw_zones` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
