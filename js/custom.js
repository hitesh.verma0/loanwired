function confirmAction(href, msg,title='Confirmation',status='success',showCancelButton=false) {
    var defaultMsg = 'Are you sure? you want perform this action.';
    defaultMsg = typeof msg !== 'undefined' ? msg : defaultMsg;
     swal.fire({
        title:title,
        text:defaultMsg,
        type: status,
        showCancelButton: showCancelButton,
        cancelButtonText: 'cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false,
        icon: status,
     }).then((obj) => {
        if(obj.value==true){
          location.href = href;
          return true;
      }
    });
    return false;
}
 
 

function reasonMessage(href="") { 
         var locale = {
            OK: 'I Suppose',
            CONFIRM: 'Send',
            CANCEL: 'Cancel'
        };
        bootbox.addLocale('custom', locale);
        bootbox.prompt({ 
            inputType: 'textarea',
            title: "Please share the reason for disapproval.", 
            locale: 'custom',
            callback: function (result) {
                if(result!=null)
                   location.href = href+'?reason='+result;
                   return true;  
                // console.log('This was logged in the callback: ' + result);
            }
        });
        return false;
}
/*ajax start stop loading*/

function showLoadingIndicator() {
    html = '<div class="spinner-container"><div class="loader-main"><div class="cssload-loader"><div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div></div><p>Please wait ...</p></div></div>'; 
    $('body').append(html);
}

function hideLoadingIndicator() {
    $('.spinner-container').remove();
}

$(document).ajaxStart(function () {
    showLoadingIndicator();
});

$(document).ajaxComplete(function (event, xhr, settings) {
    hideLoadingIndicator();
});


function error_check(jqXHR, exception) {
    var msg = error_messages(jqXHR, exception);
    message_alert(msg, 'error');
}

function error_messages() {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
    }
    return msg;
}
 
$(document).on('blur', '.fields', function () {
    var obj = $(this);
    update_fields(obj);
});

function message_box(text, heading, icon="info",position="top-right"){
    $.toast({
        heading: heading,
        text: text,
        position:position,
        stack: false,
        icon: icon
    });
}

function update_fields(id,model,field,value) { 
    $.ajax({
        url: SITEURL + 'ajax/update_fields',
        type: 'POST',
        data: {
            id: id,
            model: model,
            value: value,
            field: field
        },
        //async: false,
        success: function (response) {
            var obj = jQuery.parseJSON(response);
            if ($.trim(obj.status)) {
                messgae_box(obj.message, obj.heading, obj.icon);
            }
        }
    });
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function goBack() {
  window.history.back();
}

function setCookie(cname, cvalue,exdays='1') {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname){
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

   
function validationrecaptcha() {
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
       return "false";
    }
    return "true";
} 

function get_institutes(model,field,value,reponseFieldId){
    if(value!=''){
        $.ajax({
            type: 'GET',
            url: SITEURL+'ajax/get_institutes',
            data: {form_model:model,filterby:field,filtervalue:value}, 
            success: function (response) {
               $('#'+reponseFieldId).html(response);
            } 
        });
    }
}



 
function getauthordetail(userid) {
    if(userid!=''){
        $.ajax({
            type: 'GET',
            url: SITEURL+'ajax/userdetail',
            data: {username:userid}, 
            success: function (response) {
               var obj = jQuery.parseJSON(response);
                if (obj.status!='failed') {
                    swal.fire({
                        // title:obj.name,
                        title:"Teacher Details",
                        icon: 'info',
                        html:obj.html,
                         width: '650px',
                        showCloseButton: true,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }
            } 
        });
    }
} 

 

function changeStatus(id,status,model) {
     $.ajax({
        url: SITEURL+'ajax/change_status/',
        type: 'POST',
        data:{id:id,model:model,status:status},
        success: function (response) {
        if(response!=0){   
         $("#change_status_"+id).replaceWith(response);
        }else{
           alert("This is special news.please remove from special news than Inactive.");
          }
      }  
    });
}


function confirmAction(href, msg,title='Confirmation',status='success',showCancelButton=false) {
    var defaultMsg = 'Are you sure? you want perform this action.';
    defaultMsg = typeof msg !== 'undefined' ? msg : defaultMsg;
     swal.fire({
        title:title,
        text:defaultMsg,
        type: status,
        showCancelButton: showCancelButton,
        cancelButtonText: 'cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false,
        icon: status,
     }).then((obj) => {
        if(obj.value==true){
          location.href = href;
          return true;
      }
    });
    return false;
}

function submitConfirm(form, msg,title='Confirmation',status='question',showCancelButton=true) {
    var defaultMsg = 'Are you sure? you want perform this action.';
    defaultMsg = typeof msg !== 'undefined' ? msg : defaultMsg;
     swal.fire({
        title:title,
        text:defaultMsg,
        type: status,
        showCancelButton: showCancelButton,
        cancelButtonText: 'cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false,
        icon: status,
     }).then((obj) => {
        if(obj.value==true){
          form.submit();
      }
    });
    return false;
} 


function submitloginbymob(){
    $("#loginbymob").submit();
}

jQuery(document).ready(function($) {
    // Scroll to the desired section on click
    // Make sure to add the `data-scroll` attribute to your `<a>` tag.
    // Example: 
    // `<a data-scroll href="#my-section">My Section</a>` will scroll to an element with the id of 'my-section'.
    function scrollToSection(event) {
      event.preventDefault();
      var $section = $($(this).attr('href')); 
      $('html, body').animate({
        scrollTop: $section.offset().top
      }, 500);
    }
    $('[data-scroll]').on('click', scrollToSection);
  }(jQuery));

 
  //Get the button
  var mybutton = document.getElementById("myBtn");
  
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function() {scrollFunction()};
  
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  }
  
  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }  