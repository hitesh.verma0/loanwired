<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('loan_file_prefix','20')->unique();
            $table->text('description');
            $table->float('amount', 8, 2);
            $table->string('age_to_apply');
            $table->float('interest')->default("0.10");
            $table->string('image')->nullable();
            $table->string('image_path')->nullable();
            $table->tinyInteger('status')->default("1");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('products');
    }
}
