<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_users', function (Blueprint $table) {
            $table->id();
            $table->integer('role_id')->default("4")->foreignId('role_id')->constrained('roles')->onDelete('cascade'); 
            $table->string('emp_id');
            $table->string('name');
            $table->string('password');
            $table->tinyInteger('gender')->default("1")->comment('1=>Male,2=>Female,3=>Transgender');
            $table->date('dob');
            $table->string('father_name');
            $table->string('email')->unique();
            $table->string('mobile')->unique(); 
            $table->string('present_address')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('profile')->nullable();
            $table->date('doj');
            $table->integer('designation_id');
            $table->string('reporting_manager');
            $table->tinyInteger('zone_id');
            $table->tinyInteger('branch_id');
            $table->tinyInteger('area_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_users');
    }
}
