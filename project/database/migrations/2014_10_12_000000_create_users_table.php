<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('role_id')->default("4")->foreignId('role_id')->constrained('roles')->onDelete('cascade'); 
            $table->string('name')->nullable();;
            $table->string('fname')->nullable();
            $table->string('midname')->nullable();
            $table->string('lname')->nullable();
            $table->string('mobile','12')->unique();
            $table->string('email')->unique()->nullable(); 
            $table->tinyInteger('gender')->default("1")->comment('1=>Male,2=>Female,3=>Transgender');
            $table->date('dob')->nullable();
            $table->integer('age')->nullable();
            $table->string('password')->nullable();
            $table->char('otp','4')->nullable();
            $table->tinyInteger('sent_otp_number')->nullable();
            $table->tinyInteger('has_mobile_verified')->default("0");
            $table->string('aadhar_num')->unique()->nullable();; 
            $table->string('pan_num')->unique()->nullable();;
            $table->string('pincode','6')->nullable();;
            $table->string('profile')->nullable();
            $table->string('image_path')->nullable();;
            $table->tinyInteger('status')->default("1"); 
            $table->tinyInteger('register_from')->default("1")->comment('1=>Fronted,2=>Admin-panel');; 
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken()->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
