<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('field_name')->nullable();
            $table->integer('document_categories')->default("1"); 
            $table->string('ext_allow')->nullable();
            $table->tinyInteger('order_num')->default("1"); 
            $table->tinyInteger('is_required')->default("0"); 
            $table->tinyInteger('status')->default("1"); 
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('documents');
    }
}
