<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_documents', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->foreignId('user_id')->constrained('users')->onDelete('cascade'); 
            $table->integer('application_id')->foreignId('application_id')->constrained('applications')->onDelete('cascade'); 
            $table->string('field_name')->nullable();
            $table->string('document_path')->nullable();
            $table->string('document')->nullable();
            $table->tinyInteger('status')->default("1"); 
            $table->tinyInteger('approve_by')->default("0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_documents');
    }
}
