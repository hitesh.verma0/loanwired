<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->foreignId('user_id')->constrained('users')->onDelete('cascade'); 
            $table->integer('employment_type_id')->nullable()->foreignId('employment_type_id')->constrained('employment_types')->onDelete('cascade'); 
            $table->integer('product_id')->default("1")->foreignId('product_id')->constrained('products')->onDelete('cascade')->comment('Loan Type');
            $table->integer('bank_id')->nullable()->nullable()->foreignId('bank_id')->constrained('banks')->onDelete('cascade'); 
            $table->integer('loan_file_num')->nullable();
            $table->tinyInteger('salary_type')->default("1"); 
            $table->tinyInteger('property_type')->default("1"); 
            $table->tinyInteger('step_completed')->default("1"); 
            $table->string('company_name')->nullable();
            $table->string('salary_monthly')->nullable();
            $table->string('professional_email')->nullable();
            $table->float('loan_amount',8,2)->nullable();
            $table->string('account_number',"50")->nullable();
            $table->string('ifsc_code',"20")->nullable();
            $table->string('netbanking_uname',"200")->nullable();
            $table->string('netbanking_pass',"200")->nullable();
            $table->tinyInteger('has_monthly_statment')->default("1");
            $table->tinyInteger('bank_statment_type')->default("1")->comment('1=>Bank statement,2=>Net banking');;
            $table->string('next_step','20')->nullable();
            $table->rememberToken()->nullable();;
            $table->tinyInteger('status')->default("2"); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
