<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationerequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_erequests', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->foreignId('user_id')->constrained('users')->onDelete('cascade'); 
            $table->integer('application_id')->foreignId('application_id')->constrained('applications')->onDelete('cascade'); 
            $table->string('api')->nullable();
            $table->string('session_id')->nullable();
            $table->string('approved_limit')->nullable();
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->string('response_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_erequests');
    }
}
