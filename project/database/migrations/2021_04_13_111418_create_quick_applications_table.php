<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuickApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('quick_applications', function (Blueprint $table) {
            $table->id();
            $table->string('mobile','12')->unique();
            $table->char('otp','4')->nullable();
            $table->tinyInteger('sent_otp_number')->nullable(); 
            $table->tinyInteger('has_mobile_verified')->default("0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quick_applications');
    }
}
