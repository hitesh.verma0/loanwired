<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_products', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->foreignId('product_id')->constrained('products')->onDelete('cascade')->comment('FK');
            $table->integer('document_id')->foreignId('document_id')->constrained('documents')->onDelete('cascade')->comment('FK ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_products');
    }
}
