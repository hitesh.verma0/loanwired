<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Providers\RouteServiceProvider; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\QuickApplication;
use Exception;
use Config;
class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth')->except("sentotp","verifyotp","resendotp","login","quickapply","create_user");
    }
    
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function my_account(){
        return view('my_account');
    } 

    
    public function sentotp(){
        try{    
                $redirect="loan/apply";
                $isTempApplication=false; 
                $isTimerStart=false;
                if($this->request->isMethod("post")){
                    $isTimerStart=true;
                    $data = $this->request->post(); 
                    $this->validate($this->request, ['mobile' => 'required|min:10|max:10']);
                    $user = DB::table('users')->where('mobile',$data["mobile"])->first();
                    if(empty($user)){
                        $user=$this->checkUserInQuickApplication($data["mobile"]);
                        $isTempApplication=true;
                        // $user = DB::table('quick_applications')->where('mobile',$data["contactNo"])->first();
                    }
                    $otpNumber=(empty($user->sent_otp_number))?"0":$user->sent_otp_number;
                    if(!$this->checkNumberOfRequestForOtp($otpNumber)){
                        throw new Exception(__('You have tried many time. Please contact with loanwired team.')); 
                    }
                    if(!empty($user)){  
                        $otp = rand(1111, 9999);
                        $otpMsg=(config('app.sms.otp_msg'))?config('app.sms.otp_msg'):"Dear Applicant, Your One Time Password (OTP) for Loan Registration is #var#. Do not share this with anybody";  
                        $sms=str_replace("#var#",$otp,$otpMsg);
                        $this->_sendSMS(trim($data["mobile"]),$sms); 
                        $otpSentLimit=(!empty($user->sent_otp_number))?($user->sent_otp_number+1):1;
                        $updateFields=['otp' =>$otp,"sent_otp_number"=>$otpSentLimit,"updated_at"=>date("Y-m-d H:i:s")];
                        if($isTempApplication){ // Check User in Quick Application Table
                            $affected = DB::table('quick_applications')->where('id',$user->id)->update($updateFields);
                        }else{
                            $affected = DB::table('users')->where('id',$user->id)->update($updateFields); 
                        } 
                        $this->request->session()->put('umob',$data["mobile"]);
                    }else{
                        throw new Exception(__('Your are not registred with '.config('app.name').'.')); 
                    }
                }
            if($this->request->session()->exists('umob')) {
                $mobile=$this->request->session()->get('umob');
            }
            return view("sentotp",compact("mobile","redirect","isTimerStart")); 
        }catch(Exception $e){ 
            return back()->withErrors(['mobile' =>$e->getMessage()]);
            exit;
        }
    }

    public function verifyotp(){
        try{  
            $isTempApplication=false;
            $user = DB::table('users')->where('mobile',$this->request["mobile"])->first(); 
            if(empty($user)){
                $user=$this->checkUserInQuickApplication($this->request["mobile"]);
                $isTempApplication=true; 
            } 
            if(empty($user)){ 
                throw new Exception(__('Invalid page Request!')); 
            }   
            $endTime=strtotime($user->updated_at.'+24 minute');
            $currentTime=time();  
            if($user->otp!=$this->request["otp"]){
                throw new Exception(__('Invalid OTP!')); 
            }else{ 
                if($endTime <= $currentTime){
                    throw new Exception(__('You OTP has been expired!')); 
                }else{ 
                    if($isTempApplication){   
                        $svFields=["mobile"=>trim($user->mobile),"has_mobile_verified"=>1,'otp' =>$user->otp,"sent_otp_number"=>NULL];
                        if(User::create($svFields)){
                            $deletedRows = QuickApplication::where('id',$user->id)->delete();
                        }
                        unset($user);
                        $user=DB::table('users')->where('mobile',$this->request["mobile"])->first(); 
                    }   
                    if (Auth::loginUsingId($user->id,true)) { 
                        $updateFields=['otp' =>NULL,"sent_otp_number"=>NULL]; 
                        $affected = DB::table('users')->where('id',$user->id)->update($updateFields);  
                        $redirectFrontedUser=(isset($this->request["redirect"])&&!empty($this->request["redirect"]))?$this->request["redirect"]:"/";
                        return ($user->role_id==4)?redirect()->intended("/$redirectFrontedUser"):redirect()->intended('/admin/dashboard');
                    }
                 }
            } 
        }catch(Exception $e){  
            return back()->withErrors(['otp' =>$e->getMessage()]);
            exit;
        }
    }

    public function resendotp(){ 
        $response['status']="success";
        $response['message']="One time password Message Resent successfully.";
        try{  
            if($this->request->ajax()){
                $isTempApplication=false;
                $data = $this->request->post();  
                $user = DB::table('users')->where('mobile',$data["contactNo"])->first();
                if(empty($user)){
                    $user=$this->checkUserInQuickApplication($data["contactNo"]);
                    $isTempApplication=true;
                    // $user = DB::table('quick_applications')->where('mobile',$data["contactNo"])->first();
                } 
                $otpNumber=(empty($user->sent_otp_number))?"0":$user->sent_otp_number;
                if(!$this->checkNumberOfRequestForOtp($user->sent_otp_number)){
                    throw new Exception(__('You have tried many time. Please contact with loanwired team.')); 
                }
                if(!empty($user)){  
                    $otp = rand(1111, 9999);
                    $otpMsg=(config('app.sms.otp_msg'))?config('app.sms.otp_msg'):"Dear Applicant, Your One Time Password (OTP) for Loan Registration is #var#. Do not share this with anybody";  
                    $sms=str_replace("#var#",$otp,$otpMsg);
                    $this->_sendSMS(trim($data["contactNo"]),$sms); 
                    $otpSentLimit=(!empty($user->sent_otp_number))?($user->sent_otp_number+1):1;
                    //$this->sendOTP($user->mobile); 
                    // $password= Hash::make($otp); ,"password"=>$password
                    $updateFields=['otp' =>$otp,"sent_otp_number"=>$otpSentLimit,"updated_at"=>date("Y-m-d H:i:s")];
                    if($isTempApplication){ // Check User in Quick Application Table
                        $affected = DB::table('quick_applications')->where('id',$user->id)->update($updateFields);
                    }else{
                        $affected = DB::table('users')->where('id',$user->id)->update($updateFields); 
                    } 
                    $this->request->session()->put('umob',$data["contactNo"]);
                }
            } 
        }catch(Exception $e){  
            $response['status']="failed";
            $response['message']=$e->getMessage();
        }
        echo json_encode($response);
        die;
    }

    private function checkNumberOfRequestForOtp($count=null){ 
        if( !empty($count) &&  $count > 5){
            return false;
        }
        return true;
    }

    public function login(){
        return view('login');
    } 

    function checkUserInQuickApplication($mobile){
        $user = DB::table('quick_applications')->where('mobile',$mobile)->first(); 
        return $user;
    }
     
    public function quickapply(){
        try{  
                $isTempApplication=false;
                $isTimerStart=false;
                if($this->request->isMethod("post")){
                    $isTimerStart=true;
                    $data = $this->request->post(); 
                    $this->validate($this->request, 
                    ['mobile' => ['required','string','regex:/^([0-9\s\-\+\(\)]*)$/','size:10']],
                    [ 
                        'mobile.required' => 'This field is required.',
                        'mobile.regex' => 'Mobile number invalid.',
                    ]);
                    $user = DB::table('users')->where('mobile',$data["mobile"])->first();
                    
                    if(empty($user)){
                        $user=$this->checkUserInQuickApplication($data["mobile"]);
                        $isTempApplication=true;
                    }
                    // $otpNumber=(empty($user->sent_otp_number))?"0":$user->sent_otp_number;
                    // if(!$this->checkNumberOfRequestForOtp($otpNumber)){
                    //     throw new Exception(__('You have tried many time. Please contact with loanwired team.')); 
                    // } 
                    $otp = rand(1111, 9999);
                    $otpMsg=(config('app.sms.otp_msg'))?config('app.sms.otp_msg'):"Dear Applicant, Your One Time Password (OTP) for Loan Registration is #var#. Do not share this with anybody";  
                    $sms=str_replace("#var#",$otp,$otpMsg);
                    if(!empty($user)){                        
                         
                        $this->_sendSMS(trim($data["mobile"]),$sms); 
                        $otpSentLimit=(!empty($user->sent_otp_number))?($user->sent_otp_number+1):1; 
                        // $password= Hash::make($otp); ,"password"=>$password
                        $updateFields=['otp' =>$otp,"sent_otp_number"=>$otpSentLimit,"updated_at"=>date("Y-m-d H:i:s")];
                        if($isTempApplication){ // Check User in Quick Application Table
                            $affected = DB::table('quick_applications')->where('id',$user->id)->update($updateFields);
                        }else{
                            $affected = DB::table('users')->where('id',$user->id)->update($updateFields); 
                        }
                    }else{                         
                        $this->_sendSMS(trim($data["mobile"]),$sms);
                        $svFields=['mobile' =>$data["mobile"],'otp' =>$otp,"sent_otp_number"=>1,"has_mobile_verified"=>"0"];
                        QuickApplication::create($svFields);  
                        //throw new Exception(__('Your are not registred with '.config('app.name').'.')); 
                    }
                    $this->request->session()->put('umob',$data["mobile"]); 
                }
            if($this->request->session()->exists('umob')) {
                $mobile=$this->request->session()->get('umob');
            }
            $redirect="loan/apply"; 
            return view("sentotp",compact("mobile","redirect","isTimerStart")); 
        }catch(Exception $e){ 
            return back()->withErrors(['mobile' =>$e->getMessage()]);
            exit;
        }
    } 
}