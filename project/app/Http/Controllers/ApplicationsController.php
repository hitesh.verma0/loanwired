<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Providers\RouteServiceProvider; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Html\FormFacade;
use Illuminate\Html\HtmlFacade;
use Carbon\Carbon;
use Image;
use DateTime;
use App\Models\User;
use App\Models\Application;
use App\Models\ApplicationErequest;
use App\Models\EmploymentType;
use App\Models\ApplicationDocument;  
use App\Traits\CustomTrait;  
// use ThirdPartyApi;

use Route;
use Exception;

class ApplicationsController extends Controller
{   use CustomTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth'); //->except() 
       
    } 
    
    public function checkStepAndRedirect(string $action,string $nextStep= null,$application=""){ 
        $id="";   
        if(empty($nextStep)){
            $nextStep="apply";   
        }  
        if($action!=$nextStep&&$application->status!=2){ // status 2 mean form under process
              return true;
        }
        return false;
    }

    public function apply(){  
        $userId=auth()->user()->id;
        $user = User::find($userId); //
        // $application = Application::whereIn([['status',"IN",["2","0","4"]],['user_id',"=",$user->id]])->orderBy('id', 'desc')->first();
        $application=DB::table('applications')->whereIn('status',["2","0","4"])->where('user_id',$user->id)->orderBy('id', 'desc')->first();
        
        $id=(!empty($application))?base64_encode($application->id):"";  
        $stepCompleted=(!empty($application))?$application->step_completed:""; 
        $product =(!empty($id))?Application::find($application->id)->product:"";  
        $productName =(!empty($product))?$product->name:"";
        
        $nextStep=(!empty($application))?$application->next_step:"";  
        if($this->checkStepAndRedirect($this->action,$nextStep,$application)){  
            return redirect('loan/'.$nextStep.'/'.$id);
        }  

        if($this->request->isMethod("put")){
            $data = $this->request->post();    

            $age=$this->calculateAge($data['dob']);  
            $tweentyYearAgoDate=Carbon::now()->subYears(20)->toDateString();
            // dd($tweentyYearAgoDate);
            $this->request["dob"]=(isset($data['dob'])&&!empty($data['dob']))?$this->dateFormat($data['dob']):"";
            $validate=$this->validate($this->request, [
                'fname' => ['required', 'string','alpha', 'max:255'],
                'midname' => ['nullable', 'string','alpha', 'max:255'],
                'lname' => ['required', 'string','alpha','max:255'],
                'dob' => ['required', 'date',"before_or_equal:$tweentyYearAgoDate"],
                'email' => ['required', 'string', 'email', 'max:255','unique:users,email,'.$user->id,],
                // 'mobile' => ['required', 'string','regex:/^([0-9\s\-\+\(\)]*)$/', 'size:10', 'unique:users'],
                'pincode' => ['required', 'string','regex:/^([0-9\s\-\+\(\)]*)$/','min:5','max:6'],
                'aadhar_num' => ['required', 'string','regex:/^([0-9\s\-\+\(\)]*)$/','min:12','max:12','unique:users,aadhar_num,'.$user->id],
                'pan_num' => ['required', 'string','regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/','max:191','unique:users,pan_num,'.$user->id,],
                'product_id' => ['required', 'numeric','min:1'], 
                ],
                [
                    'email.unique' => 'This email already registred.',
                    // 'dob.before' => 'You are not eligible for loan.',
                    'aadhar_num.unique' => 'This Aadhar number already registred.',
                    'pan_num.unique' => 'This Pan number already used.',
                    'pan_num.regex' => 'Invalid pan number.',
                    'pincode.min' => 'Please enter valid pincode.',
                    'pincode.max' => 'Please enter valid pincode.', 
                    'aadhar_num.min' => 'Please enter valid aadhar number.',
                    'pan_num.max' => 'Please enter valid pan number.',
                    'product_id.required' => 'This field is required.',
                    'product_id.numeric' =>'Please select valid loan type.',
                    'product_id.min' => 'Please select valid loan type.',
                    'mobile.required' => 'This field is required.',
                    'fname.required' => 'This field is required.',
                    'lname.required' => 'This field is required.',
                    'dob.required' => 'This field is required.',
                    'email.required' => 'This field is required.', 
                    'pincode.required' => 'This field is required.',
                    'aadhar_num.required' => 'This field is required.',
                    'fname.alpha_dash' => 'Only Alpha,Numeric,dashes and underscores are allow.',
                    'midname.alpha_dash' => 'Only Alpha,Numeric,dashes and underscores are allow.',
                    'lname.alpha_dash' => 'Only Alpha,Numeric,dashes and underscores are allow.',
                    'pan_num.required' => 'Only Alpha,Numeric,dashes and underscores are allow.', 
                ]
            ); 
            // dd($data);
            // $user = DB::table('users')->where('mobile',$data["mobile"])->first();
            $mid=(!empty($data['midname']))?" ".$data['midname']." ":" ";
            $name=$data['fname'].$mid.$data['lname'];
            $saveData=[
                'name' =>$name,
                'fname' =>$data['fname'],
                'midname' =>$data['midname'],
                'lname' =>$data['lname'],
                'gender' =>$data['gender'],
                'email' => $data['email'],
                'dob' =>$this->dateFormat($data['dob']), 
                'age'=>$age,
                'pincode' => $data['pincode'], 
                'aadhar_num' => $data['aadhar_num'], 
                'pan_num' => $data['pan_num'], 
            ]; 
            $saveData=$this->removeScriptTag($saveData,'single');
            DB::table('users')->where('id',$user->id)->update($saveData); 
             
            $saveAppData=[ 
                'user_id' =>$user->id,
                'product_id' =>$data['product_id'],
                'step_completed'=>1,
                'next_step'=>'professional-details'
            ]; 
            $saveAppData=$this->removeScriptTag($saveAppData,'single');
            if(empty($application)){
                $appId=Application::create($saveAppData)->id;
                $this->_generateLoanFileNumber($appId); 
                $id=base64_encode($appId);
            }else{
                DB::table('applications')->where('id',$application->id)->update(['product_id' =>$data['product_id']]);
            }
            return redirect()->intended('loan/professional-details/'.$id)->with('success','Personal details updated successfully.');
        }   
        return view("registration.create_user",compact('application','user',"id","stepCompleted","productName")); // ,compact("mobile")        
    }


    public function professional_details($id=""){ 
        $userId=auth()->user()->id;
        $application=[];
        $application = Application::where([['status',"=","2"],['user_id',"=",$userId]])->orderBy('id', 'desc')->first();
        $stepCompleted=(!empty($application))?$application->step_completed:"";
        $nextStep=(!empty($application))?$application->next_step:"";  
        $product =(!empty($application))?Application::find($application->id)->product:"";  
        $productName =(!empty($product))?$product->name:"";
        if($this->checkStepAndRedirect($this->action,$nextStep,$application)){  
            return redirect('loan/'.$nextStep.'/'.$id);
        }
        if($this->request->isMethod("put") || $this->request->isMethod("post")){
            $data = $this->request->post();    
            
            $validate=$this->validate($this->request, [
                'employment_type_id' => ['required', 'numeric'], 
                ], 
                [ 
                    'employment_type_id.required' => 'This field is required.',
                 ]
            ); 
                
            if(isset($data['employment_type_id'])&&!empty($data['employment_type_id'])&&$data['employment_type_id']==1){
                $validate=$this->validate($this->request, [
                    'company_name' => ['required', 'string','alpha_spaces','max:255'], 
                    // 'salary_monthly' => ['required', 'string','regex:/^([0-9\s\-\+\(\)]*)$/','min:5','max:10'],
                    'salary_monthly' => ['required', 'not_in:0','numeric','min:20000','max:1000000000'],
                    'professional_email' => ['required', 'string', 'email', 'max:255'], 
                    ], 
                    [ 
                        'company_name.required' => 'This field is required.', 
                        'company_name.alpha_spaces' => 'Invalid company name.', 
                        'professional_email.required' => 'This field is required.', 
                        'salary_monthly.min' => 'Salary should not below 20k.',
                        'salary_monthly.max' => 'Please enter valid salary.',
                     ]
                );
            }
             $saveData=[
                'employment_type_id' =>$data['employment_type_id'],
                'user_id' =>$userId,
                'salary_type' =>$data['salary_type'],
                'salary_monthly' =>$data['salary_monthly'],
                'company_name' =>$data['company_name'],
                'professional_email' => $data['professional_email'], 
                'step_completed'=>2,
                'next_step'=>'congratulations'
            ];
            $saveData=$this->removeScriptTag($saveData,'single');
            if(!empty($application)){
                $id=base64_encode($application->id);
                DB::table('applications')->where('id',$application->id)->update($saveData); 
            }else{
                $appId=Application::create($saveData)->id; 
                $id=base64_encode($appId); // Using for generate loan file number
            } 

            return redirect()->intended('loan/congratulations/'.$id)->with('success');
            /*
            if(!empty($application->employment_type_id)){ 

                
            }else{
                return redirect()->intended('loan/professional-details/'.$id.'/#doc')->with('success');
            } 
            */   
        }  
        $professionalDocuments=["first_salary_slip","second_salary_slip","thired_salary_slip"]; 
        $file_max_size =2048;
        $file_min_size =2;
        $photo_max_size =500;
        $photo_min_size =1; 
        $appDocuments=$this->_getLoanDocumentList($application->id); 
        $uploadedDocuments=$this->_getLoanDocuments($application->id,$professionalDocuments);
        return view("registration.professional_details",compact("productName","appDocuments",'id','application',"stepCompleted",'professionalDocuments',"uploadedDocuments","photo_max_size","photo_min_size","file_max_size","file_min_size"));
    }


    function congratulations($id=""){ 
        $appId=base64_decode($id);
        $userId=auth()->user()->id;
        $data=$jsonResponse=$invalidMsg="";
        $application=[];
        if(!empty($id)){
            $application = Application::find($appId);
        }
        $stepCompleted=(!empty($application))?$application->step_completed:"";
        $nextStep=(!empty($application))?$application->next_step:"";  
        if($this->checkStepAndRedirect($this->action,$nextStep,$application)){  
            return redirect('loan/'.$nextStep.'/'.$id);
        }
        $product =(!empty($id))?Application::find($application->id)->product:"";  
        $employmentType =(!empty($application))?Application::find($application->id)->EmploymentType:"";
        $productName =(!empty($product))?$product->name:"";
        $user = User::find($userId);
        $gender=$this->getGender($user->gender);
        // $user->pan_num="ABFPB1509L";
        $requestEligibility=[
            "pan"=> strtoupper($user->pan_num),
            "first_name"=> ucfirst($user->fname),
            "last_name"=>ucfirst($user->lname),
            "date_of_birth"=>$user->dob,
            "employment_type"=>$employmentType->name,
            "gender"=>$gender,
            "monthly_income"=>$application->salary_monthly,
            "postal_code"=>$user->pincode
        ]; 
        // $user->mobile="9000003383"; //ABFPB1509L
        $data = DB::table('application_erequests')->where([['user_id',$userId],['application_id',$appId],['api',"check_eligibility"]])->whereNotNull('session_id')->orderBy('id', 'desc')->first(); 
        $fromApi=true;
        if(empty($data)){
            $createUserResp = DB::table('application_erequests')->where([['user_id',$userId],['application_id',$appId],['api',"uat_create_user"]])->orderBy('id', 'desc')->first();  
            if(!empty($createUserResp)){ 
                $isuatValid=$this->uatValidateRecord($createUserResp);
                if($isuatValid["status"]==true){
                    $endTime=strtotime($user->updated_at.'+10 minute');
                    $currentTime=time();  
                    if($endTime <= $currentTime){
                        $loginRequestData =["phone"=>"+91".$user->mobile]; 
                        $sessionId=$this->userUatLogin($loginRequestData,$userId,$appId);
                    }else{
                        $sessionId=$createUserResp->session_id;
                    } 
                    $jsonResponse=$this->checkUatElig($requestEligibility,$userId,$appId,$sessionId); 
                }else{
                    $invalidMsg=$isuatValid["message"]." ! Please contact with ".config('app.name')." team.";  
                }
            }else{ // Register Api
                $requestData =["phone"=>"+91".$user->mobile,"email"=>$user->email,"terms_accepted"=> 'true',"phone_verified"=>'true'];
                $uatApi=(!empty(env('UAT_API')))?env('UAT_API'):"https://api.paysense.io/users/v1/external/users";	
                $response= $this->uatApiCall($requestData,$uatApi);
                $saveData=[ 
                    'api' =>"uat_create_user",
                    'request' =>json_encode($requestData),
                    'response' =>json_encode($response),
                ];
                if(!empty($response)&&isset($response->details[0])&&$response->details[0]->code=="invalid"){
                    $saveData['response_status']= $response->details[0]->code;
                    $err=(isset($response->details[0]->message)&&!empty($response->details[0]->message))?$response->details[0]->message:"Technical error";
                    if(str_contains($err,'user is already registered')){ 
                        $loginRequestData =["phone"=>"+91".$user->mobile]; 
                        $sessionId=$this->userUatLogin($loginRequestData,$userId,$appId);
                        $jsonResponse=$this->checkUatElig($requestEligibility,$userId,$appId,$sessionId);  
                    }else{
                        $invalidMsg =$err." ! Please contact with ".config('app.name')." team.";
                        $this->saveErequestData($userId,$appId,$saveData,"uat_create_user");
                    }
                }else{ 
                    $responseArr=(array) $response; 
                    $sessionId=$saveData['session_id']= $responseArr["session-token"]; 
                    $saveData['response_status']="success"; 
                    $this->saveErequestData($userId,$appId,$saveData,"uat_create_user");  
                    $jsonResponse=$this->checkUatElig($requestEligibility,$userId,$appId,$sessionId);
                }
            }
        }else{
            $fromApi=false;
            $jsonResponse=(!empty($data)&&!empty($data->response))?json_decode($data->response):"";   
        }  
        

        if(!empty($jsonResponse)&&isset($jsonResponse->is_declined)&&$jsonResponse->is_declined){
            $invalidMsg =$this->whyDeclined($jsonResponse->is_declined);
            $invalidMsg .=" ! Please contact with ".config('app.name')." team.";
        }elseif(isset($jsonResponse->code)&&!empty($jsonResponse->code)&&$jsonResponse->code=="invalid"){
            if($jsonResponse->details[0]->field=="pan"){
                    $err="Pan card not valid.";
            }else{
                $err=(isset($jsonResponse->details[0]->message)&&!empty($jsonResponse->details[0]->message))?$jsonResponse->details[0]->message:"Technical error";    
            }
			$invalidMsg=$err." ! Please contact with ".config('app.name')." team."; 
        }
        return view("registration.congratulations",compact("jsonResponse","invalidMsg","productName",'application','id','appId',"stepCompleted","user"));
    }
    public function banking_statement($id=""){
        $appId=base64_decode($id);
        $userId=auth()->user()->id;
        $application=[];
        $application = Application::find($appId); 
        $nextStep=(!empty($application))?$application->next_step:"";  
        $stepCompleted=(!empty($application))?$application->step_completed:"";

        $product =(!empty($application))?Application::find($application->id)->product:"";  
        $productName =(!empty($product))?$product->name:"";

        if($this->checkStepAndRedirect($this->action,$nextStep,$application)){  
            return redirect('loan/'.$nextStep.'/'.$id);
        }
        $appDocuments=$this->_getLoanDocumentList($application->id); 
        $banks=$this->_getbankList();
        if($this->request->isMethod("post")){ 
            $data = $this->request->post();     
            $validate=$this->validate($this->request, [
                'account_number' => ['required', 'string','regex:/^([0-9\s\-\+\(\)]*)$/','max:25'], 
                'bank_id' => ['required', 'numeric'], 
                'bank_statment_type' => ['required', 'numeric'], 
                'ifsc_code' => ['required', 'string','regex:/^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/','max:20'],  //SBIN1234567
                ], 
                [ 
                    'bank_id.required' => 'This field is required.',
                    'bank_statment_type.required'=>'This field is required.',
                    'account_number.required' =>'This field is required.',
                    'account_number.regex' => 'Invalid account number.',
                    'ifsc_code.required' =>'This field is required.',
                    'ifsc_code.regex' => 'Invalid IFSC code number.',
                ]
            );   
            $aid=base64_decode($data['aid']);
            $saveData=[
                'account_number' =>$data['account_number'],
                'ifsc_code' =>$data['ifsc_code'],
                'bank_id' =>$data['bank_id'],
                'bank_statment_type' =>$data['bank_statment_type'],
                'step_completed'=>4, 
                'next_step'=>'kyc-documents'
                // 'netbanking_uname' =>$data['netbanking_uname'] 
            ];
            $saveData=$this->removeScriptTag($saveData,'single');
            DB::table('applications')->where('id',$aid)->update($saveData);
            
            if(isset($application->account_number) && !empty($application->account_number)){
                return redirect()->intended('loan/kyc-documents/'.$data['aid'])->with('success');
            }else{
                return redirect()->intended('loan/banking-statement/'.$data['aid'])->with('success');
            }
        }  
        $bankingDocuments=["three_months_statment","first_month_statment","second_month_statment","thired_month_statment"]; 
        $uploadeArrFields=(!empty($application->has_monthly_statment)&&$application->has_monthly_statment==1)?["three_months_statment"]:["first_month_statment","second_month_statment","thired_month_statment"];
        $uploadedDocuments=$this->_getLoanDocuments($application->id,$uploadeArrFields);
        $file_max_size =2048;
        $file_min_size =2;
        $photo_max_size =500;
        $photo_min_size =20; 
        return view("registration.banking_statement",compact("productName",'uploadedDocuments','application','banks','id',"bankingDocuments","appDocuments","photo_max_size","photo_min_size","file_max_size","file_min_size","stepCompleted"));
    }

    function kyc_document($id=""){
        $appId=base64_decode($id);
        $userId=auth()->user()->id;
        $application=[];
        $application = Application::find($appId);
        $stepCompleted=(!empty($application))?$application->step_completed:"";
        $nextStep=(!empty($application))?$application->next_step:"";  
        $product =(!empty($application))?Application::find($application->id)->product:"";  
        $productName =(!empty($product))?$product->name:"";
        if($this->checkStepAndRedirect($this->action,$nextStep,$application)){  
            return redirect('loan/'.$nextStep.'/'.$id);
        }
        if($this->request->isMethod("post")){ 
            $data = $this->request->post();     
            $validate=$this->validate($this->request, [
                'property_type' => ['required', 'numeric'], 
                ], 
                [ 
                    'property_type.required' => 'This field is required.',
                ]
            );  
            $aid=base64_decode($data['aid']);
            $saveData=[
                'property_type' =>$data['property_type'],
                'step_completed'=>5,
                'next_step'=>'thank-you'
            ]; 
            $saveData=$this->removeScriptTag($saveData,'single');
            $saveData["status"]=0;
            DB::table('applications')->where('id',$aid)->update($saveData); 
            return redirect()->intended('loan/thank-you/'.$data['aid'])->with('success');
        }  
        $file_max_size =2048;
        $file_min_size =2;
        $photo_max_size =500;
        $photo_min_size =1;
        $kycDocuments=["applicant_photo","electricity_bill","other_proof","aadhar_card_front","aadhar_card_back","pan_card","first_salary_slip","second_salary_slip","thired_salary_slip"]; 
        $appDocuments=$this->_getLoanDocumentList($application->id);
        $uploadedDocuments=$this->_getLoanDocuments($application->id,$kycDocuments);
        
        // $professionalDocuments=["first_salary_slip","second_salary_slip","thired_salary_slip"];
        
        return view("registration.kyc_document",compact("productName","uploadedDocuments",'application','id',"stepCompleted",'appDocuments','kycDocuments',"photo_max_size","photo_min_size","file_max_size","file_min_size"));
    } 
    
    public function uploadfiles(){
            $file_max_size =2048;
            $file_min_size =2;
            $photo_max_size =500;
            $photo_min_size =20;
            // dd($this->request->file());
            //  dd($this->request->post()); 
            $postData=$this->request->post();
        try{    
            if(!$this->request->isMethod("post")){ 
                throw new Exception(__('Invalid page Request!'));  
            } 
            $appId=base64_decode($postData['aid']);
            $userId=auth()->user()->id; 
            $application = Application::find($appId); 
            $product = Application::find($appId)->product; 
             
            if(empty($application) || empty($product)){
                throw new Exception(__('Invalid Request!'));  
            } 
            $fieldname=(isset($postData["fieldname"]))?$postData["fieldname"]:"";
            $kycDocuments=["applicant_photo","electricity_bill","other_proof","aadhar_card_front","aadhar_card_back","pan_card"]; 
            $professionalDocuments=["first_salary_slip","second_salary_slip","thired_salary_slip"]; 
            $bankingDocuments=["three_months_statment","first_month_statment","second_month_statment","thired_month_statment"]; 
            
            $documents= array_merge($kycDocuments,$professionalDocuments,$bankingDocuments);   
            if(!isset($fieldname) || empty($fieldname) || !in_array($fieldname,$documents)){
                throw new Exception(__('Technical error! Please contact with '.config('app.name').' team.'));  
            }
            $fieldname=$postData["fieldname"];
            
            if(in_array($fieldname,$bankingDocuments)){
                $this->request->validate([
                    $fieldname =>"required|mimes:pdf|max:$file_max_size",
                ]);
            }elseif(in_array($fieldname,["applicant_photo"])){
                $this->request->validate([
                    $fieldname =>"required|image|mimes:jpeg,png,jpg,gif,svg|max:$file_max_size",
                ]); 
            }else{
                $this->request->validate([
                    $fieldname =>"required|mimes:pdf,jpeg,png,jpg,gif,svg|max:$file_max_size",
                ]); 
            } 
            //https://growideportfolio.s3.ap-south-1.amazonaws.com/ 
            if($this->request->hasfile($fieldname)){   
                // var_dump($file->move(public_path('/uploads'), $fileName)); // local saved 
                $file = $this->request->file($fieldname);
                // $imageName=$file->getClientOriginalName();
                $file_name=$fieldname.'.'.$file->getClientOriginalExtension(); //'uploades/'.
                $filePath=str_replace("-","_",$product->slug)."/".$product->loan_file_prefix.$application->loan_file_num."/";
                 
                if(Storage::disk('s3')->put($filePath.$file_name, file_get_contents($file))){
                    $saveAppDocData=[ 
                        'user_id' =>$userId,
                        'application_id' =>$application->id,
                        'field_name' =>$fieldname,
                        'document' =>$file_name,
                        'document_path'=>$filePath,
                        'status' =>1, 
                    ]; 
                    //['status',"=","1"],
                    $appDoc = ApplicationDocument::where([['user_id',"=",$userId],['application_id',"=",$application->id],['field_name',"=",$fieldname]])->orderBy('id', 'desc')->first(); 
                    if(!empty($appDoc)){   
                        DB::table('application_documents')->where('id',$appDoc->id)->update($saveAppDocData); 
                    }else{
                        ApplicationDocument::create($saveAppDocData);  
                    }
                    if(in_array($fieldname,$bankingDocuments)){ 
                        $hasMonthlyStatment=($postData['monthly_statment']==1&&$fieldname=="three_months_statment")?"1":"2";
                        $saveAppData=['has_monthly_statment'=>$hasMonthlyStatment]; 
                        DB::table('applications')->where('id',$application->id)->update($saveAppData);
                    }
                         
                }  

            return back()->with('success','You have successfully upload image.');
            } 
        }catch(Exception $e){  
            $fieldname=(isset($postData["fieldname"]))?$postData["fieldname"]:"";
            return back()->withErrors([$fieldname =>$e->getMessage()]);
            exit;
        }   
    } 
    public function thank_you($id=''){
        $appId=base64_decode($id);
        $userId=auth()->user()->id;
        $application=[];
        try{    
            $application = Application::find($appId);
  
            $user = User::find($userId);
            $product =(!empty($application))?Application::find($application->id)->product:"";  
            $productName =(!empty($product))?$product->name:"";
            return view("registration.thank_you",compact('application','id',"user","productName"));
        }catch(Exception $e){  
            $fieldname=(isset($postData["fieldname"]))?$postData["fieldname"]:"";
            return back()->withErrors([$fieldname =>$e->getMessage()]);
            exit;
        } 
    }

    public function updateUploadedfile($fieldname=null,$id=null){
        $appId=base64_decode($id);

        try{  
            $userId=auth()->user()->id;
            
            $appDoc = ApplicationDocument::where([['user_id',"=",$userId],['application_id',"=",$appId],['field_name',"=",$fieldname]])->orderBy('id', 'desc')->first();
            if(!empty($appDoc)){    
                $saveAppDocData["status"]=0;
                DB::table('application_documents')->where('id',$appDoc->id)->update($saveAppDocData);
                return back()->with('success','Record Deleted successfully.');
            }
        }catch(Exception $e){   
            return back()->withErrors([$fieldname =>$e->getMessage()]);
            exit;
        }    
    }
}
