<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request; 
use Illuminate\Html\FormFacade;
use Illuminate\Html\HtmlFacade;
use Illuminate\Database\Eloquent\Model;
use App\Models\Application;
use App\Models\ApplicationDocument;
use App\Models\SmsLog; 
use Carbon\Carbon;
use Cookie; 
use View;
class Controller extends BaseController
{   
    public $request="";
    public $action="";
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(Request $request){ 
        $this->request=$request;
        $action=$this->request->route()->getActionMethod();  
        $this->action=$action;
        View::share('action', $action);
    } 
    
    public function writeCookie($cookieName,$cookieVal,$minutes){
        $cookie = cookie($cookieName,$cookieVal, $minutes);
        return response($cookieVal)->cookie($cookie); 
    }

    public function _sendSMS($mobile,$msg=""){ 
        if(!empty($msg)){
            $msg=urlencode($msg); 
            $api=(config('app.sms.api'))?config('app.sms.api'):"http://164.52.205.46:6005/api/v2/SendSMS?SenderId=GROWID&ApiKey=ySIIkhd6P0VntPrXouTsCBtl2XuYwk%2BhovvP1jEENT0%3D&&ClientId=0a23ce98-3d66-4d25-a404-204a2409697e&Is_Unicode=false&Is_Flash=false";
            $url=$api."&MobileNumbers=".$mobile."&Message=$msg"; 
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_POST, true); 
            curl_setopt($ch, CURLOPT_TIMEOUT,60); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            $response = curl_exec($ch); 
            $err = curl_error($ch); 
            curl_close($ch); 
            if(env('SMS_LOG')){
                $sv["request"]=json_encode(array("sms"=>$msg,"mobile"=>$mobile));
                $sv["mobile"]=$mobile;
                $sv["response"]=$response;
                SmsLog::create($sv);
            }
        }
	   return true;
    }


    protected function _generateLoanFileNumber(int $id){
        $fileNumber=(10000+$id);
        DB::table('applications')->where('id',$id)->update(["loan_file_num"=>$fileNumber]);
        return true;
    }

    public function _getLoanFileNumber(){
        
    }

    public function _getLoanDocumentList($applicaionId){
        $list=[];
        $list = DB::table('application_documents')->where([['application_id',$applicaionId],['status',1]])->pluck("field_name",'id')->toArray();
        return $list;
    }

    public function _getLoanDocuments($applicaionId,$fields=""){
        $response = DB::table('application_documents')->whereIn('field_name',$fields)->where('application_id',$applicaionId)->where('status',1)->get()->toArray();
        return $response;
    }

    public function _getbankList(){
        $list=[];
        $response = DB::table('banks')->where([['status',1]])->orderBy('name','asc')->pluck("name",'id');
        if(!empty($response)){
            foreach ($response as $id=>$name) {
                $list[$id]=$name;
            }
        }
        return $list;
    }

       /*
   * @params: 
   * @Function use: removeScriptTag: this function use to remove html from field
   * @created by: Hitesh verma
   * @Created: 20-04-2021
   */
    public function removeScriptTag($dataArr='',$ArrType="multiple"){
        $newArr=array();
        if($ArrType=="multiple"){
            foreach ($dataArr AS $key=>$data){
                foreach ($data as $index =>$value) { 
                    $newArr[$key][$index]=(!empty($value)&&!is_array($index))?strip_tags(trim($value)):null;
                }
            }
        }else{ 
            foreach ($dataArr as $index =>$value) {
                $newArr[$index]=(!empty($value)&&!is_array($index))?strip_tags(trim($value)):null;
            }
        }  
        return $newArr;
    }

    public function dateFormat($dateString) {
		$date = str_replace("/","-",$dateString);
		$datetime =  date("Y-m-d",strtotime($date));
		return $datetime;
    }

    public function calculateAge($dob){ 
        $dob=$this->dateFormat($dob);
        return $age = Carbon::parse($dob)->age;   
    }
    
   
}

