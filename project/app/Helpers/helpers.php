<?php 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

if (! function_exists('customHelperGetLastMonthName')) {
    function customHelperGetLastMonthName(int $leastCount){ 
        return date("F", strtotime("-$leastCount months"));
    }
}

if (! function_exists('customHelperDateFormat')) {
    function customHelperDateFormat($dateString) {
        $datetime="";
        if(!empty($dateString)){
            $date = str_replace("/","-",$dateString);
            $datetime =  date("d/m/Y",strtotime($date));
        }
        return $datetime;
    }
}
if (! function_exists('customHelperDocumentLink')) {
    function customHelperDocumentLink($file,$url="",$thirdLink='false',$class='btn-link',$label='View Document',$title='View Document',$width="100%",$height="auto"){ 
        if(empty($file)&&$thirdLink!=false){
            $document="<a  class='$class' title='$title' target='_blank' href='$url'>$label</a>";
            return $document;
        }
            if(!empty($file)){
            $fileTypeArr=explode("/",customHelperFileContentType($file));
            $file_type=$fileTypeArr['0'];
            if($file_type=='application'){
            $file_type=$fileTypeArr['1'];
            } 
            $url=($url!="")?Storage::disk('s3')->url($url):"javascript:void(0)"; 
            switch ($file_type) {
                case 'image':  
                    $document="<a class='fancybox $class' title='$title' href='$url' data-fancybox data-type='image'><img src='$url' width='$width' height='$height' /> </a>";
                break;
                case 'pdf':
                    $document="<a  class='iframfancybox mr-2' data-fancybox  data-type='iframe' href='$url'><i style='font-size: 40px; color: red; ' class='document-icon text-red fa fa-file-pdf-o'></i></a>";
                break;
                case 'docx':
                    $document="<a  class='$class' href='$url' title='$title' download='download'>$label</a>";
                break;
                case 'video':
                    $document="<a class='$class fancybox' title='$title' data-type='video' data-fancybox data-width='800' data-height='450' href='$url'>$label</a>";
                break;
                default:
                    $document="<a class='$class' href='$url' title='$title' target='_blank'>$label</a>";
                break;
            }
            return $document;
        }
        return "#";
        
    }
}    
    
if (! function_exists('customHelperDocumentIcon')) { 
    function customHelperDocumentIcon($file,$url="javascript:void(0)"){ 
        $document='<i class="document-icon fa fa-file" aria-hidden="true"></i>';
        if(!empty($file)){ 
            $fileTypeArr=explode("/",customHelperFileContentType($file));
            $file_type=$fileTypeArr['0'];
            if($file_type=='application'){
            $file_type=$fileTypeArr['1'];
            } 
            switch ($file_type) {
                case 'image':  
                    $document="<im";    
                // $document="<i class='document-icon fa fa-file-image-o'></i>";
                break;
                case 'pdf':
                $document="<i class='document-icon fa fa-file-pdf-o'></i>";
                break;
                case 'docx':
                $document="<i class='document-icon fa fa-file-doc-o'></i>";
                break;
                case 'video':
                $document="<i class='document-icon fa fa-file-video'></i>";
                break;
                default:
                $document="<i class='document-icon fa fa-file'></i>";
                break;
            }

        }
        return $document;
    }    
}
if (! function_exists('customHelperFileContentType')) { 
    function customHelperFileContentType($filename) {
        $filename=strtolower($filename);
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'mp4' => 'video/x-mp4',
            'avi' => 'video/x-avi',
            'flv' => 'video/x-flv',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/docx',
            'docx' =>'application/docx',
            'rtf' => 'application/docx',
            'xls' => 'application/docx',
            'ppt' => 'application/docx',
            'csv' => 'application/docx',
            'pptx' => 'application/docx',
            'xlsx' => 'application/docx',
            // open office
            'odt' => 'application/docx',
            'ods' => 'application/docx',
        );
        //$ext = strtolower(array_pop(explode('.',$filename)));
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }else {
            return 'application/octet-stream';
        }
    }
}

if (! function_exists('multiDimationalSearch')) { 
    
    function multiDimationalSearch($arr,$value,$arrayIndex){ 
        $key="";
        $key = array_search($value, array_column($arr,$arrayIndex));
        return (isset($arr[$key])&&!empty($arr[$key]))?$arr[$key]:"";

    } 
}