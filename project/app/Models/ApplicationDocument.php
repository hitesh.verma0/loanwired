<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationDocument extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'application_id',
        'field_name',
        'document', 
        'document_path',
        'status', 
        'approve_by',
        'updated_at',  
    ]; 
}
