<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuickApplication extends Model{

    use HasFactory;
    
    protected $fillable = [
        'mobile',
        'otp',
        'sent_otp_number',
        'has_mobile_verified',  
    ]; 
}
