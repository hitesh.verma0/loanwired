<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;
    protected $fillable = [
        'employment_type_id',
        'user_id',
        'product_id',
        'bank_id',
        'salary_type',
        'salary_monthly',  
        'company_name',  
        'professional_email',
        'property_type',
        'next_step',
        'loan_amount',
        'account_number',
        'ifsc_code',
        'netbanking_uname',
        'netbanking_pass',
        'has_monthly_statment',
        'bank_statment_type',
        'loan_file_num'
    ]; 

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'netbanking_pass', 
    ];

    public function product()
    {
        return $this->BelongsTo(Product::class,'product_id');
    }

    public function EmploymentType(){
        return $this->BelongsTo(EmploymentType::class);
    }
}
