<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationErequest extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'application_id',
        'api',
        'session_id', 
        'approve_limit',
        'response', 
        'request',
        'response_status', 
        'created_at',
        'updated_at',
    ]; 
}
