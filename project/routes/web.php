<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 
Route::get('/', function () {
    return view('home');
}); 
Route::get('/login', [App\Http\Controllers\UsersController::class, 'login'])->name('login'); 
// Route::get('/admin', [App\Http\Controllers\admin\StaffUsersController::class, 'admin_login'])->name('admin_login'); 

Route::get('/admin', [App\Http\Controllers\admin\StaffUsersController::class, 'admin_login'])->name('admin_login')->middleware(["Admin"]); 
Route::namespace('admin')->prefix('admin')->group(function () { 
    Route::get('/dashboard', [App\Http\Controllers\admin\StaffUsersController::class, 'dashboard'])->middleware(["Admin"]); ; 
});
   



 
// Route::any('/create-user/', [App\Http\Controllers\UsersController::class,'create_user'])->name('create_user');  
Route::post('/verifyotp', [App\Http\Controllers\UsersController::class, 'verifyotp'])->name('verifyotp'); 
Route::any('/sentotp', [App\Http\Controllers\UsersController::class, 'sentotp'])->name('sentotp');
Route::post('/resendotp', [App\Http\Controllers\UsersController::class, 'resendotp'])->name('resendotp'); 
Route::any('/quickapply/', [App\Http\Controllers\UsersController::class,'quickapply'])->name('quickapply'); 
 

Route::namespace('loan')->prefix('loan')->group(function () { 
    Route::any('/apply', [App\Http\Controllers\ApplicationsController::class,'apply'])->name('apply');
    Route::any('/professional-details/{id}', [App\Http\Controllers\ApplicationsController::class,'professional_details'])->name('professional_details');
    Route::any('/congratulations/{id}', [App\Http\Controllers\ApplicationsController::class,'congratulations'])->name('congratulations');
    Route::any('/banking-statement/{id}', [App\Http\Controllers\ApplicationsController::class,'banking_statement'])->name('banking_statement');
    Route::any('/kyc-documents/{id}', [App\Http\Controllers\ApplicationsController::class,'kyc_document'])->name('kyc_document');
    Route::any('/thank-you/{id}', [App\Http\Controllers\ApplicationsController::class,'thank_you'])->name('thank_you');
    Route::any('/update-upload-file/{field_name}/{id}', [App\Http\Controllers\ApplicationsController::class,'updateUploadedfile'])->name('updateUploadedfile');
    Route::any('/uploadfiles', [App\Http\Controllers\ApplicationsController::class,'uploadfiles'])->name('uploadfiles');
});  
Route::get('/my-account', [App\Http\Controllers\UsersController::class, 'my_account'])->name('my_account');

