@extends('layouts.login')

@section('content')
    <section class="login-section">
        <div class="login-div">
            <div class="left-logo-img">
                <a href="{{ url('/')}}">
                <i><img src="images/login-bg.jpg"  alt=""/></i>
                <span><img src="images/login-bg-side.jpg"  alt=""/></span>
                </a>
            </div>  
            <div class="form-login"><div class="mid-form">
                <div class="heading-se"><h5>Welcome Back</h5>
                    <i>Get an Instant Loan</i></div>
                    <div class="login-form-sec">
                        <b>Log In With Your Mobile Number</b>
                        <form action="{{'sentotp'}}" method="post">
                            {{ csrf_field() }}
                            <div class="mobile-no">
                                <em>Mobile Number</em>
                                <i>+91</i><input type="text" name="mobile" class="{{ $errors->has('mobile') ? 'is-invalid' : '' }}"
                                    value="{{ old('mobile') }}" onkeypress="return isNumber();" placeholder="Mobile Number..." pattern="\d*" maxlength="10" required="required">
                                @if($errors->has('mobile'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </div>
                                @endif
                            </div> 
                            <button type="submit">SEND OTP</button>
                        </form> 
                        {{-- <p>Don’t have an account? <a href="#">Create an account</a> </p> --}}
                        <p>By Proceeding, you agree with our <a href="#">Terms and Conditions</a> and <a href="#">Privecy Policy</a>.</p>
                    </div>
                </div>
            </div> 
        </div>
    </section> 
@endsection
