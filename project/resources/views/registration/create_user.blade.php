@extends('layouts.register')

@section('content')

<section class="kyc-bg">
    <div class="container"> 
      <div class="gry-bg">
        <div class="row">
          <div class="col-xs-12">
            <div class="heading- personal-icon">
              <h1>Personal Details</h1>
            </div>
            <form action="{{'apply'}}" method="post">
            {{ csrf_field() }} 
            @method('PUT')
            <!-- List group -->
            <div class="tabpanal">
              @php $fieldName="gender";  
              // $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:"";
              $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:"1"; 
              $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
            @endphp
              <div class="list-group" id="myList" role="tablist"> <b class="pro-head">Select your Gender</b>
                <div class="opt-01"> 
                  <input name="gender" type="radio" value="1" {{ ((empty(old('gender')) || old('gender')==1) || ($fieldVal==1) )? "checked" : "" }}>
                  <a class="list-group-item list-group-item-action" data-toggle="list" href="#home" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/male-active.png')}}"> <img class="hideonactive"  width="100px" src="{{asset('/images/male-inactive.png')}}"> <b>Male</b> </a>
                </div>
                <div class="opt-01">
                  <input name="gender" type="radio" value="2"  {{ ((old('gender')==2) || ($fieldVal==2) )? "checked" : "" }} > <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/female-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/female-inactive.png')}}"> <b>Female</b> </a>
                </div>
                <div class="opt-01">
                  <input name="gender" type="radio" value="3" {{ ((old('gender')==3) || ($fieldVal==3) )? "checked" : "" }}>
                  <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/transgender-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/transgender-inactive.png')}}"> <b>Transgender</b></a>
                </div>
              </div>
              <div class="input-section pan-det">
              <b class="pro-head">Full Name as per PAN Card</b>
              <div class="row">
              
              <div class="col-md-4">
              <div class="input-design">
              <label>First name <span class="text-danger">*</span></label>
              @php $fieldName="fname";
               $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
               $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
              @endphp
                  <input type="text" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" maxlength="255" >
                  @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
              </div> 
              </div>
              <div class="col-md-4">
              <div class="input-design">
              <label>Middle name</label>
              @php $fieldName="midname";
              $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
               $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
              @endphp
                  <input type="text" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" maxlength="255">
                  @if($errors->has($fieldName))
                      <div class="invalid-feedback text-danger">
                          <strong>{{ $errors->first($fieldName) }}</strong>
                      </div>
                  @endif
                </div>
              
              </div>
              <div class="col-md-4">
              <div class="input-design">
              <label> Last name <span class="text-danger">*</span></label>
              @php $fieldName="lname";
              $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
               $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
              @endphp
                <input type="text" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" maxlength="255" >
                  @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
                </div>
              </div>
              
              
              <div class="col-md-4">
              <div class="input-design">
              <label>Date of birth <span class="text-danger">*</span></label>
              @php $fieldName="dob";
              $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
               $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
              @endphp
              <input type="text"  name="dob" class="datepicker {{ $errors->has('dob') ? 'is-invalid' : '' }}" value="{{customHelperDateFormat($fieldVal)}}" maxlength="10" readonly>
              <i>DD/MM/YYYY</i>
              @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
              </div> 
              </div>
              <div class="col-md-4">
              <div class="input-design">
              <label> Pincode</label>
                  @php $fieldName="pincode";
                  $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
                $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                  @endphp
                  <input type="text" name="{{$fieldName}}" maxlength="6" miinlength="5" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" >
              <i>As per current address proof</i>
              @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
              </div> 
              </div>
              <div class="col-md-4">
              <div class="input-design">
              <label> Email <span class="text-danger">*</span></label>
                @php $fieldName="email";
                $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
               $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                @endphp
                  <input type="email" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" maxlength="255" >
                  @if($errors->has($fieldName))
                  <div class="invalid-feedback text-danger">
                      <strong>{{ $errors->first($fieldName) }}</strong>
                  </div>
                @endif
              </div> 
              </div>
              <div class="clearfix"></div>
              {{-- <div class="col-md-4">
                <div class="input-design">
                      <label>Mobile <span class="text-danger">*</span></label>
                      @php $fieldName="mobile"; @endphp
                      <input type="text" name="{{$fieldName}}" maxlength="10" miinlength="10" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ old($fieldName) }}" >
                      @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
                </div> 
              </div> --}}
              <div class="col-md-4">
                <div class="input-design">
                  <label>Aadhar no</label>
                  @php $fieldName="aadhar_num";
                  $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
               $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                  @endphp
                    <input type="text" name="{{$fieldName}}" maxlength="12" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" >
                    @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
                </div> 
              </div>
                <div class="col-md-4">
                  <div class="input-design">
                    @php $fieldName="pan_num";
                    $fieldVal=(!empty($user->{$fieldName}))?$user->{$fieldName}:""; 
                    $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                    @endphp
                        <label>PAN No <span class="text-danger">*</span></label>
                        <input type="text" name="{{$fieldName}}" pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" maxlength="12" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" >
                        @if($errors->has($fieldName))
                    <div class="invalid-feedback text-danger">
                        <strong>{{ $errors->first($fieldName) }}</strong>
                    </div>
                  @endif
                  </div> 
                </div>
                <div class="col-md-4">
                  <div class="input-design"> 
                        <label>Loan Type <span class="text-danger">*</span></label>
                        @php $fieldName="product_id"; 
                         $loanVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"1"; 
                        $loanVal=(!empty(old($fieldName)))?old($fieldName):$loanVal; 
                        @endphp
                        {!! Form::select($fieldName, ['1' => 'Personal Loan', '2' =>'Business Loan'], $loanVal,["onchange"=>"loantype(this.value)","id"=>"loantypeoption"]); !!} 
                        @if($errors->has($fieldName))
                      <div class="invalid-feedback text-danger">
                          <strong>{{ $errors->first($fieldName) }}</strong>
                      </div>
                    @endif
                  </div> 
                </div> 

                <div class="clearfix"></div>
                  <div class="upload-btn">
                        <button type="submit"><?php echo "CONTINUE"; //CONTINUE ?></button>
                  </div>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection 