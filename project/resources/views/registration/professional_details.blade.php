@extends('layouts.register')

@section('content')

<section class="kyc-bg">
    <div class="container"> 
      <div class="gry-bg completed-sec">
        <div class="row">
          <div class="col-xs-12">
            <div class="heading-">
              <h1>Personal Details</h1>
            </div> 
             
          </div>
        </div>
      </div>
      <div class="gry-bg">
        <div class="row">
          <div class="col-xs-12">
            <div class="heading- personal-icon">
              <h1>Professional Details</h1>
            </div>
            <form action="{{'professional-details'}}" method="post" id="adddataform">
            {{ csrf_field() }} 
            @method('post')
            <!-- List group -->
            <div class="tabpanal">
              @php $fieldName="employment_type_id";  
                  // $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"";
                  $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"1"; 
                  $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
              @endphp
              <div class="list-group" id="employmentTypeIdList" role="tablist"> <strong class="pro-head">Select employment type</strong>
                <div class="opt-01"> 
                  <input name="{{$fieldName}}" type="radio" value="1" {{ ((empty(old($fieldName)) || old($fieldName)==1) || ($fieldVal==1) )? "checked" : "" }}>
                  <a class="list-group-item list-group-item-action" data-toggle="list" href="#home" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/salaried-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/salaried-inactive.png')}}"> <b>Salaried</b> </a>
                </div>
                {{-- <div class="opt-01">
                  <input name="{{$fieldName}}" type="radio" value="2"  {{ (old($fieldName)==2 || ($fieldVal==2))? "checked" : "" }} > <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/self-emploed-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/self-emploed-inactive.png')}}"> <b>Self Emploed</b> </a>
                </div>  --}}
              </div>
              <div class="list-group" id="salaryPaymentList" role="tablist">
                <b class="pro-head">Select mode of salary payment</b>
                @php $fieldName="salary_type"; 
                  // $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"";
                  $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"1"; 
                  $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                @endphp 
                  <div class="opt-01">
                    <input name="{{$fieldName}}" type="radio" value="1" {{ ((empty(old($fieldName)) || old($fieldName)==1) || ($fieldVal==1))? "checked" : "" }}>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#home" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/online-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/online-inactive.png')}}"> <b>Online</b> </a></div>
                  <div class="opt-01">
                    <input name="{{$fieldName}}" type="radio" value="2" {{ (old($fieldName)==2  || ($fieldVal==2) )? "checked" : "" }}>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/cheque-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/cheque-inactive.png')}}"><b>Cheque</b> </a> </div>
                  <!--<div class="opt-01">
                    <input name="{{$fieldName}}" type="radio" value="3" {{ (old($fieldName)==3  || ($fieldVal==3) )? "checked" : "" }}>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/cash-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/cash-inactive.png')}}"> <b>Cash</b> </a> </div>-->
                </div>
              <div class="input-section pan-det"> 
              <div class="row">
                
                <div class="col-md-4">
                  <div class="input-design">
                      <label>Company Name <span class="text-red">*</span></label>
                      @php
                      $fieldName="company_name";
                      $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:""; 
                      $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                      @endphp
                        <input type="text" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{$fieldVal}}" maxlength="255" >
                        @if($errors->has($fieldName))
                          <div class="invalid-feedback text-danger">
                              <strong>{{ $errors->first($fieldName) }}</strong>
                          </div>
                        @endif
                  </div> 
                </div>
                <div class="col-md-4">
                    <div class="input-design">
                    <label>Monthly take Home Salary <span class="text-danger">*</span></label>
                      @php $fieldName="salary_monthly";
                      $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:""; 
                      $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                      @endphp
                        <input type="text" name="{{$fieldName}}"  onkeypress="return isNumber();"  class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" maxlength="10">
                        @if($errors->has($fieldName))
                            <div class="invalid-feedback text-danger">
                              <strong>{{ $errors->first($fieldName) }}</strong>
                          </div>
                      @endif
                    </div>
                
                </div>
                <div class="col-md-4">
                  <div class="input-design">
                  <label>Proffesional Email<span class="text-danger">*</span></label>
                  @php
                    $fieldName="professional_email";
                    $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:""; 
                    $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                  @endphp
                      <input type="email" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" maxlength="255">
                      @if($errors->has($fieldName))
                          <div class="invalid-feedback text-danger">
                              <strong>{{ $errors->first($fieldName) }}</strong>
                          </div>
                      @endif
                    </div>
                  
                  </div> 
                
                  </div>
                </div> 
                <div class="upload-btn">
                  <button  type="submit" >CONTINUE </button>
                </div> 
                @php  /*if(empty($application->employment_type_id)){ @endphp
               
                @php } */ @endphp
              </div>
              {{-- <button type="submit" id="formsubmit" >CONTINUE </button> --}}
            </form>
            @php /*if(!empty($application->employment_type_id)){ @endphp
            @php
              }*/
            @endphp
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
    function saveform(){  
      var totalSalarySlipUploaded=$(".salarycount").length; 
      if(totalSalarySlipUploaded >=3){
        $('form#adddataform').submit();
      }else{
        message_box('Please upload salary slip!',"Alert!","warning","mid-center");  
      }
    } 

  $(function () {	
  $('.loandocument').on('change', function() {
		var ext = $(this).val().split('.').pop().toLowerCase(); 
		if($.inArray(ext, ['pdf','gif','png','jpg','jpeg']) == -1) {
			message_box('Invalid extension!! Please upload file with extension pdf, gif, png, jpg, jpeg!',"Alert!","warning","mid-center");  
			$(this).parents('form.loandocumentform')[0].reset();
			return false;
		}
		var fileKbSize = (this.files[0].size / 1024);
		if (fileKbSize > <?php echo $file_max_size;?>) { //greater than 1.5 MB
				$(this).val('');
				message_box('Size exceeded !! Please upload file with less than <?php echo $file_max_size;?>kb!',"Alert!","warning","mid-center"); 
				$(this).parents('form.loandocumentform')[0].reset();
				return false;
		}
		if (fileKbSize <= <?php echo $file_min_size;?> ) { //greater than 1.5 MB
				$(this).val('');
				message_box('content: Please upload file with size greater than <?php echo $file_min_size;?>kb!',"Alert!","warning","mid-center"); 
				$(this).parents('form.loandocumentform')[0].reset();
				return false;
		}
		if($(this).val()!=''){
			 showLoadingIndicator();
			$(this).parents('form.loandocumentform').submit();
		}
	}); 
}); 

  </script>
  @endsection 