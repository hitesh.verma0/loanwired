@extends('layouts.register') 
@section('content')
<section class="kyc-bg">
  <div class="container">
    <div class="gry-bg white-bg">
      <div class="row">
          <div class="col-xl-12 text-center">
            <div class="Congres-user">
              @if (!empty($invalidMsg))
              <div class="Congres-img">
                <img src="{{asset('/images/rejected-page.jpg')}}">
                <p>{{ $invalidMsg }}</p>
              </div>
              @else
              <div class="Congres-img">
                <img src="{{asset('/images/star-img.png')}}">
              </div>
              <div class="Congres-contant-btns">
                <h2>Congratulations, {{ucfirst($user->name)}}</h2>
                <span>Pre-Approved!</span>
                <a href="#" class="check-icon"><i class="fa fa-check"></i></a>
                <b>Rs.  @if(!empty($jsonResponse->approved_limit))  {{ $jsonResponse->approved_limit }}  @endif </b>     
                <p>Your loan eligibility is powered by <br> Loan Wired Fintech Pvt. Ltd </p>
                <a href="{{url("/loan/banking-statement/$id")}}" class="Continue-btn">Continue</a>
                <i>To proceed we need your KYC and Financial details</i> 
              </div>
              @endif
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
@endsection 