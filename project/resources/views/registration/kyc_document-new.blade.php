@extends('layouts.register')
@section('content')
<section class="kyc-bg">
    <div class="container">
      <div class="gry-bg completed-sec">
        <div class="row">
          <div class="col-xs-12">
            <div class="heading-">
              <h1>Banking Statement Details</h1>
            </div> 
             
          </div>
        </div>
      </div>
      <div class="gry-bg">
        <div class="row">
          <div class="col-xs-12">
            <div class="heading- personal-icon">
              <h1>KYC Documents</h1>
            </div>
            <form action="" id="adddataform" method="post">
            {{ csrf_field() }}  
            <input type="hidden" name="aid" value="{{$id}}">
            <!-- List group -->
            <div class="tabpanal">
              @php $fieldName="property_type"; 
               
                $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"1"; 
                $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
              @endphp
              <div class="list-group" id="employmentTypeIdList" role="tablist"> <strong class="pro-head">Property Type</strong>
                <div class="opt-01"> 
                  <input name="{{$fieldName}}" type="radio" value="1" {{ ((empty(old($fieldName)) || old($fieldName)==1) || ($fieldVal==1) )? "checked" : "" }}>
                  <a class="list-group-item list-group-item-action" data-toggle="list" href="#home" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/own-property-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/own-property-inactive.png')}}">  <b>OWN PROPERTY</b> </a>
                </div>
                <div class="opt-01">
                  <input name="{{$fieldName}}" type="radio" value="2"  {{ ((old($fieldName)==2) || ($fieldVal==2)  )? "checked" : "" }} > <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab"><img class="showonactive" width="100px" src="{{asset('/images/rented-active.png')}}"> <img  class="hideonactive" width="100px" src="{{asset('/images/rented-inactive.png')}}"> <b>RENTED</b> </a>
                </div> 
              </div> 
            </div>
            </form>    
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="col-xs-6"> <b class="small-head">Electricity Bill</b>
                      @php $fieldName="electricity_bill"; 
                       
                        $isFileUploaded=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false;
                        @endphp
                      <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                        <div class="form-group files @php if($isFileUploaded){ echo 'active';} @endphp">
                          <input type="hidden" name="aid" value="{{$id}}">
                          <input type="hidden" name="fieldname" value="{{$fieldName}}">
                          @php
                           $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name");
                            $uploadClass=($isFileUploaded)?"hide":"show";
                            $uploadFileClass=($isFileUploaded)?"show":"hide";
                          @endphp
                          <div class="{{ $uploadFileClass }}" id="{{$uploadFileClass}}-{{ $fieldName }}" style="width: 100%;">
                            @php
                            if($isFileUploaded){
                              echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document,'false','btn-link','View Document','View Document',"100%","250px");    
                            }
                            @endphp
                            {{-- closeuploadedDocument --}}
                                <div class="action-btn"> <a href="#" class="check"><i class="fa fa-check" aria-hidden="true"></i></a> <span> <a href="{{ url('/loan/update-upload-file/'.$fieldName.'/'.$id) }}"  onclick="return confirmAction(this.href,'Are you sure want to DELETE this Document ?','DELETE','question','true');"  class="calce">+</a> </span> </div>
                          </div>
                          <div class="{{ $uploadClass }}" id="{{$uploadClass}}-{{ $fieldName }}">
                            <input type="file" name="{{$fieldName}}" class="loandocument">
                        
                            <label class="inp-lable form-control">
                            <center>
                              <img src="{{asset('/images/upload.png')}}" alt="">
                            </center>
                            <p> Drag and Drop your files <Br>
                              Or <Br>
                              <i>Browse</i> </p>
                              
                            </label>
                          </div>
                        </div>
                      </form> 
                      @if($errors->has($fieldName))
                      <div class="invalid-feedback text-danger">
                              <strong>{{ $errors->first($fieldName) }}</strong>
                          </div>
                      @endif
                    </div>
                    <div class="col-xs-6"> <b class="small-head">Other Proof</b>
                      @php $fieldName="other_proof";
                      $isFileUploaded=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false;
                      @endphp
                      
                      <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                        <div class="form-group files @php if($isFileUploaded){ echo 'active';} @endphp"> 
                          <input type="hidden" name="aid" value="{{$id}}">
                          <input type="hidden" name="fieldname" value="{{$fieldName}}">
                          @php
                          $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name");
                         $uploadClass=($isFileUploaded)?"hide":"show";
                         $uploadFileClass=($isFileUploaded)?"show":"hide";
                         @endphp
                         <div class="{{ $uploadFileClass }}" id="{{$uploadFileClass}}-{{ $fieldName }}" style="width: 100%;">
                           @php
                           if($isFileUploaded){
                           echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document,'false','btn-link','View Document','View Document',"100%","250px");    
                           }
                           @endphp
                               <div class="action-btn"> <a href="#" class="check"><i class="fa fa-check" aria-hidden="true"></i></a> <span> <a href="{{ url('/loan/update-upload-file/'.$fieldName.'/'.$id) }}"  onclick="return confirmAction(this.href,'Are you sure want to DELETE this Document ?','DELETE','question','true');"  class="calce">+</a> </span> </div>
                         </div>
                         <div class="{{ $uploadClass }}" id="{{$uploadClass}}-{{ $fieldName }}">

                          <input type="file" name="{{$fieldName}}" class="loandocument">
                          <label class="inp-lable form-control">
                          <center>
                            <img src="{{asset('/images/upload.png')}}" alt="">
                          </center>
                          <p> Drag and Drop your files <Br>
                            Or <Br>
                            <i>Browse</i> </p> 
                          </label>
                         </div>
                        </div> 
                      </form>
                      @if($errors->has($fieldName))
                      <div class="invalid-feedback text-danger">
                              <strong>{{ $errors->first($fieldName) }}</strong>
                          </div>
                      @endif
                    </div> 
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-6"> <b class="pro-head">Aadhar Card </b>
                      @php $fieldName="aadhar_card_front";
                      $isFileUploaded=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false;
                      @endphp
                      <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                        <div class="form-group files @php if($isFileUploaded){ echo 'active';} @endphp">
                        
                          <input type="hidden" name="aid" value="{{$id}}">
                          <input type="hidden" name="fieldname" value="{{$fieldName}}">
                          @php
                          $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name");
                         $uploadClass=($isFileUploaded)?"hide":"show";
                         $uploadFileClass=($isFileUploaded)?"show":"hide";
                         @endphp
                         <div class="{{ $uploadFileClass }}" id="{{$uploadFileClass}}-{{ $fieldName }}" style="width: 100%;">
                           @php
                           if($isFileUploaded){
                           echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document,'false','btn-link','View Document','View Document',"100%","250px");    
                         }
                           @endphp
                               <div class="action-btn"> <a href="#" class="check"><i class="fa fa-check" aria-hidden="true"></i></a> <span> <a href="{{ url('/loan/update-upload-file/'.$fieldName.'/'.$id) }}"  onclick="return confirmAction(this.href,'Are you sure want to DELETE this Document ?','DELETE','question','true');"  class="calce">+</a> </span> </div>
                         </div>
                         <div class="{{ $uploadClass }}" id="{{$uploadClass}}-{{ $fieldName }}">
                          <input type="file" name="{{$fieldName}}" class="loandocument">
                          <label class="inp-lable form-control">
                          <center>
                            <img src="{{asset('/images/upload.png')}}" alt="">
                          </center>
                          <p> Drag and Drop Aadhar front side <Br>
                            Or <Br>
                            <i>Browse</i> </p>
                          </label>
                         </div>
                        </div>
                      </form>
                      @if($errors->has($fieldName))
                        <div class="invalid-feedback text-danger">
                                <strong>{{ $errors->first($fieldName) }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-6"> <b class="pro-head">&nbsp;</b>
                      @php $fieldName="aadhar_card_back";
                      $isFileUploaded=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false;
                      @endphp
                      <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                        <div class="form-group files @php if($isFileUploaded){ echo 'active';} @endphp">
                      
                          <input type="hidden" name="aid" value="{{$id}}">
                          <input type="hidden" name="fieldname" value="{{$fieldName}}">
                          @php
                          $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name");
                         $uploadClass=($isFileUploaded)?"hide":"show";
                         $uploadFileClass=($isFileUploaded)?"show":"hide";
                         @endphp
                         <div class="{{ $uploadFileClass }}" id="{{$uploadFileClass}}-{{ $fieldName }}" style="width: 100%;">
                           @php
                           if($isFileUploaded){
                           echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document,'false','btn-link','View Document','View Document',"100%","250px");    
                         }
                           @endphp
                               <div class="action-btn"> <a href="#" class="check"><i class="fa fa-check" aria-hidden="true"></i></a> <span> <a href="{{ url('/loan/update-upload-file/'.$fieldName.'/'.$id) }}"  onclick="return confirmAction(this.href,'Are you sure want to DELETE this Document ?','DELETE','question','true');"  class="calce">+</a> </span> </div>
                         </div>
                         <div class="{{ $uploadClass }}" id="{{$uploadClass}}-{{ $fieldName }}">
                          <input type="file" name="{{$fieldName}}" class="loandocument">
                          <label class="inp-lable form-control">
                          <center>
                            <img src="{{asset('/images/upload.png')}}" alt="">
                          </center>
                          <p> Drag and Drop Aadhar back side <Br>
                            Or <Br>
                            <i>Browse</i> </p>
                          </label>
                         </div>
                        </div>
                      </form>
                      @if($errors->has($fieldName))
                        <div class="invalid-feedback text-danger">
                                <strong>{{ $errors->first($fieldName) }}</strong>
                            </div>
                        @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-6"> <b class="pro-head">PAN Card</b>
                      @php $fieldName="pan_card";
                      $isFileUploaded=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false;
                      @endphp
                      <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                        <div class="form-group files @php if($isFileUploaded){ echo 'active';} @endphp">
                          <input type="hidden" name="aid" value="{{$id}}">
                          <input type="hidden" name="fieldname" value="{{$fieldName}}">
                          @php
                          $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name");
                         $uploadClass=($isFileUploaded)?"hide":"show";
                         $uploadFileClass=($isFileUploaded)?"show":"hide";
                         @endphp
                         <div class="{{ $uploadFileClass }}" id="{{$uploadFileClass}}-{{ $fieldName }}" style="width: 100%;">
                            @php
                            if($isFileUploaded){
                              echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document,'false','btn-link','View Document','View Document',"100%","250px");    
                            }
                            @endphp
                              <div class="action-btn"> <a href="#" class="check"><i class="fa fa-check" aria-hidden="true"></i></a> <span> <a href="{{ url('/loan/update-upload-file/'.$fieldName.'/'.$id) }}"  onclick="return confirmAction(this.href,'Are you sure want to DELETE this Document ?','DELETE','question','true');"  class="calce">+</a> </span> </div>
                         </div>
                         <div class="{{ $uploadClass }}" id="{{$uploadClass}}-{{ $fieldName }}">
                          <input type="file" name="{{$fieldName}}" class="loandocument">
                          <label class="inp-lable form-control">
                          <center>
                            <img src="{{asset('/images/upload.png')}}" alt="">
                          </center>
                          <p> Drag and Drop your files <Br>
                            Or <Br>
                            <i>Browse</i> </p>
                          </label>
                         </div>
                        </div>
                      </form>
                      @if($errors->has($fieldName))
                        <div class="invalid-feedback text-danger">
                                <strong>{{ $errors->first($fieldName) }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-6"> <b class="pro-head">Applicant Photo </b>
                      @php $fieldName="applicant_photo";
                      $isFileUploaded=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false;
                      @endphp
                      <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                        <div class="form-group files @php if($isFileUploaded){ echo 'active';} @endphp">
                          
                          <input type="hidden" name="aid" value="{{$id}}">
                          <input type="hidden" name="fieldname" value="{{$fieldName}}">
                          @php
                          $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name");
                         $uploadClass=($isFileUploaded)?"hide":"show";
                         $uploadFileClass=($isFileUploaded)?"show":"hide";
                         @endphp
                         <div class="{{ $uploadFileClass }}" id="{{$uploadFileClass}}-{{ $fieldName }}" style="width: 100%;">
                           @php
                           if($isFileUploaded){
                           echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document,'false','btn-link','View Document','View Document',"100%","250px");   
                           } 
                           @endphp
                               <div class="action-btn"> <a href="#" class="check"><i class="fa fa-check" aria-hidden="true"></i></a> <span> <a href="{{ url('/loan/update-upload-file/'.$fieldName.'/'.$id) }}"  onclick="return confirmAction(this.href,'Are you sure want to DELETE this Document ?','DELETE','question','true');"  class="calce">+</a> </span> </div>
                         </div>
                         <div class="{{ $uploadClass }}" id="{{$uploadClass}}-{{ $fieldName }}">
                          <input type="file" name="{{$fieldName}}" class="loandocument">
                          <label class="inp-lable form-control profl-img">
                          <center>
                            <img src="{{asset('/images/user-pforile.png')}}" alt="">
                          </center>
                          <p> Drag and Drop Applicant Photo <Br>
                            Or <Br>
                            <i>Browse</i> </p>
                          </label>
                         </div>
                        </div>
                      </form>
                      @if($errors->has($fieldName))
                        <div class="invalid-feedback text-danger">
                                <strong>{{ $errors->first($fieldName) }}</strong>
                            </div>
                        @endif
                    </div>
					
					
                  </div>
                </div>
              </div>
              <div class="upload-btn">
                <button onclick="saveform()">Submit </button>
              </div>  
                   
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
      function saveform(){  
        var totaldocUploaded=$(".files.active").length;  
        if(totaldocUploaded >=6){
          $('form#adddataform').submit();
        }else{
          message_box('Please upload all document!',"Alert!","warning","mid-center");  
        }
      }
$(function () {	

  $('.loandocument').on('change', function() {
		var ext = $(this).val().split('.').pop().toLowerCase(); 
		if($.inArray(ext, ['pdf','gif','png','jpg','jpeg']) == -1) {
			message_box('Invalid extension!! Please upload file with extension pdf, gif, png, jpg, jpeg!',"Alert!","warning","mid-center");  
			$(this).parents('form.loandocumentform')[0].reset();
			return false;
		}
		var fileKbSize = (this.files[0].size / 1024);
		if (fileKbSize > <?php echo $file_max_size;?>) { //greater than 1.5 MB
				$(this).val('');
				message_box('Size exceeded !! Please upload file with less than <?php echo $file_max_size;?>kb!',"Alert!","warning","mid-center"); 
				$(this).parents('form.loandocumentform')[0].reset();
				return false;
		}
		if (fileKbSize <= <?php echo $file_min_size;?> ) { //greater than 1.5 MB
				$(this).val('');
				message_box('content: Please upload file with size greater than <?php echo $file_min_size;?>kb!',"Alert!","warning","mid-center"); 
				$(this).parents('form.loandocumentform')[0].reset();
				return false;
		}
		if($(this).val()!=''){
			 showLoadingIndicator();
			$(this).parents('form.loandocumentform').submit();
		}
	}); 
}); 

  </script>
  @endsection 