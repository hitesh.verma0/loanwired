@extends('layouts.register')

@section('content')
 {{-- Step 1 --}}
 <section class="kyc-bg">
  <div class="container">
    <div class="gry-bg">
      <div class="row">
        <div class="col-xs-12">
          <div class="heading-">
            <h1>Banking Documents</h1>
          </div>
          <!-- List group -->
          
          <div class="tabpanal">
            <form action="" method="post" id="adddataform" autocomplete="off">
              {{ csrf_field() }}  
            <input type="hidden" name="aid" value="{{$id}}">
            <div class="input-section"> <b class="pro-head">Bank Statement</b>
              <div class="row">
                <div class="col-md-6">
                  <div id="contactform">  
                   <div class="input-design"> 
                    <label>Banks <span class="text-danger">*</span></label>
                    @php $fieldName="bank_id"; 
                     $loanVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:"1"; 
                    $loanVal=(!empty(old($fieldName)))?old($fieldName):$loanVal; 
                    @endphp
                    {!! Form::select($fieldName, $banks, $loanVal); !!} 
                    @if($errors->has($fieldName))
                  <div class="invalid-feedback text-danger">
                      <strong>{{ $errors->first($fieldName) }}</strong>
                  </div>
                @endif
              </div> 
 
                    <i>Bank where you get your salary</i>
                  </div>
                  <div class="input-design mb">
                    <label>Account No</label>
                    @php $fieldName="account_number";
                    $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:""; 
                      $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                    @endphp
                    <input type="text" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{$fieldVal}}" maxlength="25" onkeypress="return isNumber();">
                    @if($errors->has($fieldName))
                      <div class="invalid-feedback text-danger">
                          <strong>{{ $errors->first($fieldName) }}</strong>
                      </div>
                    @endif
                  </div>
                  <div class="input-design  mb">
                    <label>IFSC Code</label>
                    @php $fieldName="ifsc_code";
                    $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:""; 
                      $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                    @endphp
                    <input type="text" name="{{$fieldName}}" class="{{ $errors->has($fieldName) ? 'is-invalid' : '' }}" value="{{ $fieldVal }}" autocomplete="off" maxlength="20" >
                    @if($errors->has($fieldName))
                      <div class="invalid-feedback text-danger">
                          <strong>{{ $errors->first($fieldName) }}</strong>
                      </div>
                    @endif
                  </div>
                </div>
                
                <div class="col-md-6">
                <div class="statment-icon" id="statmenttypes">
                @php $fieldName="bank_statment_type";
                  $fieldVal=(!empty($application->{$fieldName}))?$application->{$fieldName}:""; 
                  $fieldVal=(!empty(old($fieldName)))?old($fieldName):$fieldVal;
                @endphp
                <div class="st-icon01">
                <span>
                  <input name="{{$fieldName}}" type="radio" value="1" {{ ((empty(old($fieldName)) || old($fieldName)==1) || ($fieldVal==1))? "checked" : "" }}>
                <i>&nbsp;</i>
                </span>
                <div class="marg-div">
                <b>Upload Bank Statement</b>
				        <p>Manually upload your bank statements in PDF format.</p>
                </div>
                </div>
                <div class="st-icon01">
                    {{-- <span>
                      <input name="{{$fieldName}}" type="radio" value="2"  {{((old($fieldName)==2) || ($fieldVal==1))? "checked" : "" }} >
                    <i>&nbsp;</i>
                    </span>
                      <div class="marg-div"><b>Use Net Banking</b>
                        <p>Auto-upload your bank statement with Net Banking. We sill securely access only our account statement.</p>
                      </div>                 --}}
                      @if(empty($application->account_number))
                      <button type="submit">CONTINUE </button>
                      @endif
                  </div>  
                </div> 
                </div>                
              </div>
            </div>
          </form> 
          </div>
          
          @if(!empty($application->account_number))
          <div class="tabpanal"> 
              <div class="row">   
                <div class="verify-sec">
                <div class="col-md-6">
                <div class="ref-img">
                <center>
                  <img src="{{asset('/images/ref-img.jpg')}}" alt="">
                </center>
                </div>
                </div>
                <div class="col-md-6">
                  <div id="monthly_statment" class="statment-icon">
                    @php $oppFieldName="has_monthly_statment"; 
                    $oppFieldVal=(!empty($application->{$oppFieldName}))?$application->{$oppFieldName}:"1"; 
                    $oppFieldVal=(!empty(old($oppFieldName)))?old($oppFieldName):$oppFieldVal;
                    @endphp
                    @php $docfieldName="three_months_statment"; 
                    $aid=base64_encode($application->id);
                    $isFileUploaded=(!empty($appDocuments)&&in_array($docfieldName,$appDocuments))?true:false;   @endphp
                    <div class="st-icon01">
                        <span> 
                          <input name="{{$oppFieldName}}" type="radio" value="1"  {{ ((old($oppFieldName)==1) || ($oppFieldVal==1))? "checked" : "" }} >
                          <i>&nbsp;</i>
                          <p  id="uploaded-threemonthly-stetment"  class="uploadedfile"> 
                          @php if($oppFieldVal==1&&!empty($uploadedDocuments)&&$uploadedDocuments[0]->field_name=="three_months_statment"){  echo customHelperDocumentLink($uploadedDocuments[0]->document,$uploadedDocuments[0]->document_path.$uploadedDocuments[0]->document); } @endphp
                          </p>
                      </span>
                      <div class="marg-div">
                        <b>3 Months statement in 1 PDF 	</b>
                        <div id="three-month-statment" class="add-t"> 
                          @php $docfieldName="three_months_statment"; 
                            $aid=base64_encode($application->id);  @endphp
                          <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                          <input type="hidden" name="aid" value="{{$aid}}">
                          <input type="hidden" name="monthly_statment" value="1">
                          <input type="hidden" name="fieldname" value="{{$docfieldName}}">
                          <input type="file" name="{{$docfieldName}}" class="loandocument" style="width: 100%; position: absolute; opacity:0;"> 
                            <em>+</em> @php echo date("m",strtotime("-3 months"))."/".date("Y")." to ".date("m",strtotime("-1 months"))."/".date("Y"); @endphp ({{customHelperGetLastMonthName(3) }} to {{customHelperGetLastMonthName('1') }} statment)
                            @if($oppFieldVal==1&&$isFileUploaded&&count($uploadedDocuments)==1) 
                                <img class="salarycount" src="{{asset('images/check.svg')}}" alt=""> 
                              @endif 
                          </form>
                          @if($errors->has($docfieldName))
                          <div class="invalid-feedback text-danger">
                            <strong>{{ $errors->first($docfieldName) }}</strong>
                          </div>
                      @endif  
                      </div>   
                        
                      </div>
                    </div>
                    <div class="st-icon01">
                        <span> 
                          <input name="{{$oppFieldName}}" type="radio" value="2"  {{ ((old($oppFieldName)==2) || ($oppFieldVal==2))? "checked" : "" }} > 
                        <i>&nbsp;</i>
                        <p id="uploaded-monthly-stetments" class="uploaded-monthly"> 
                          @php if($oppFieldVal==2&&!empty($uploadedDocuments)){
                          foreach ($bankingDocuments as $key => $fieldName){
                          if($fieldName!="three_months_statment"){
                          $isFileUploaded1=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false; 
                          if($isFileUploaded1){
                          $uploadedDocumentSingle=multiDimationalSearch($uploadedDocuments,$fieldName,"field_name"); 
                          echo customHelperDocumentLink($uploadedDocumentSingle->document,$uploadedDocumentSingle->document_path.$uploadedDocumentSingle->document); 
                            }else{
                            echo "<a style='height:25px;'>&nbsp;</a>";
                          }
                            }
                       
                            }
                            } @endphp
                          </p>
                        </span>
                        <div class="marg-div"><b>Monthly statement PDF file</b>
                          <div id="monthlystetments" class="" style="@php if($oppFieldVal==2){ echo 'display:block;';}else{ echo 'display:none;';}    @endphp">
                            @php 
                            $leastCount=1;     
                            foreach ($bankingDocuments as $key => $fieldName){  
                              if($fieldName!="three_months_statment"){
                                  $nextMonth=$leastCount+1; 
                                  $isFileUploaded1=(!empty($appDocuments)&&in_array($fieldName,$appDocuments))?true:false; 
                                  @endphp
                                <div class="add-t"> 
                                  @php //$fieldName="first_salary_slip"; 
                                    $aid=base64_encode($application->id);
                                  @endphp
                                  <form method="post" class="loandocumentform" action="{{url('/loan/uploadfiles')}}" enctype="multipart/form-data">
                                  <input type="hidden" name="aid" value="{{$aid}}">
                                  <input type="hidden" name="monthly_statment" value="2">
                                  <input type="hidden" name="fieldname" value="{{$fieldName}}">
                                  <input type="file" name="{{$fieldName}}" class="loandocument" style="width: 100%; position: absolute; opacity:0;"> 
                                    <em>+</em> @php echo date("m",strtotime("-$leastCount months"))."/".date("Y"); @endphp ({{customHelperGetLastMonthName($leastCount) }} statment)  
                                    @if($oppFieldVal==2&&$isFileUploaded1&&count($uploadedDocuments)>1) 
                                      <img class="salarycount" src="{{asset('images/check.svg')}}" alt="">
                                    @endif 
                                  </form>
                                  @if($errors->has($fieldName))
                                        <div class="invalid-feedback text-danger">
                                          <strong>{{ $errors->first($fieldName) }}</strong>
                                        </div>
                                    @endif
                              </div> 
                              @php $leastCount++; } } @endphp 
                          </div>                
                        </div>
                    </div>
                    <div class="upload-btn">
                      <button onclick="saveform()">CONTINUE </button>
                    </div>  
                  </div>
                </div>
                </div>
                 
                 
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript" src="{{ asset('js/customSelect.jquery.min.js') }}"></script> 
<script type="text/javascript">
  (function($){  
    //Just to display the country selected (stored in a hidden input)
    $(window).click(function(e){
      $('#currentValue').html('Current value is: \''+ $('input#countrySelect')[0].value +'\'');
    });


      $('#statmenttypes input').on('change', function() {
        var st=$('input[name=bank_statment_type]:checked', '#statmenttypes').val(); 
        if(st==1){
          $("#statmenta").show();
          $("#statmentb").hide();
        }else{
          $("#statmentb").show();
          $("#statmenta").hide();
        }
      });

      $('#monthly_statment input').on('change', function() {
        var stm=$('input[name=has_monthly_statment]:checked', '#monthly_statment').val(); 
        if(stm==2){
          $("#monthlystetments").show(); 
          $("#uploaded-monthly-stetments").show(); 

          $("#three-month-statment").hide();
          $("#uploaded-threemonthly-stetment").hide();

        }else{ 
          $("#monthlystetments").hide();
          $("#uploaded-monthly-stetments").hide();

          $("#three-month-statment").show(); 
          $("#uploaded-threemonthly-stetment").show(); 
        }
      }); 

  })(jQuery);
  
  $(function () {	
  $('.loandocument').on('change', function() {
		var ext = $(this).val().split('.').pop().toLowerCase(); 
		if($.inArray(ext, ['pdf','gif','png','jpg','jpeg']) == -1) {
			message_box('Invalid extension!! Please upload file with extension pdf, gif, png, jpg, jpeg!',"Alert!","warning","mid-center");  
			$(this).parents('form.loandocumentform')[0].reset();
			return false;
		}
		var fileKbSize = (this.files[0].size / 1024);
		if (fileKbSize > <?php echo $file_max_size;?>) { //greater than 1.5 MB
				$(this).val('');
				message_box('Size exceeded !! Please upload file with less than <?php echo $file_max_size;?>kb!',"Alert!","warning","mid-center"); 
				$(this).parents('form.loandocumentform')[0].reset();
				return false;
		}
		if (fileKbSize <= <?php echo $file_min_size;?> ) { //greater than 1.5 MB
				$(this).val('');
				message_box('content: Please upload file with size greater than <?php echo $file_min_size;?>kb!',"Alert!","warning","mid-center"); 
				$(this).parents('form.loandocumentform')[0].reset();
				return false;
		}
		if($(this).val()!=''){
			 showLoadingIndicator();
			$(this).parents('form.loandocumentform').submit();
		}
	}); 
}); 
function saveform(){  
      var stm=$('input[name=has_monthly_statment]:checked', '#monthly_statment').val(); 
        if(stm==2){
          var totalSalarySlipUploaded=$(".salarycount").length; 
          if(totalSalarySlipUploaded >=3){
            $('form#adddataform').submit();
          }else{
            message_box('Please upload banking statment!',"Alert!","warning","mid-center");  
          }
        }else{ 
          var totalSalarySlipUploaded=$(".salarycount").length; 
          
          if(totalSalarySlipUploaded >=1){
            $('form#adddataform').submit();
          }else{
            message_box('Please upload banking statment!',"Alert!","warning","mid-center");  
          }
        } 
    }
  </script>
@endsection 