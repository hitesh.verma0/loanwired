<!DOCTYPE HTML>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <title>{{ config('app.name', 'Registration') }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        {{-- <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">  --}}
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{ asset('css/media.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css"> 
        <link href="{{ asset('css/css-loader.css') }}" rel="stylesheet" type="text/css"> 
        <link href="{{ asset('plugins/fancybox/fancybox.min.css') }}" rel="stylesheet" type="text/css">  
        <link href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">  
        <link href="{{ asset('vendor/sweetalert/sweetalert2.css') }}" rel="stylesheet" type="text/css">  
        <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">  
        {{-- <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">  --}} 
        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script> 
        <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('vendor/jvalidate/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('vendor/sweetalert/sweetalert2.js') }}"></script>
        <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/regiCustom.js') }}"></script>
    </head>
<body>
@include('Elements.regi-header')
@yield('content')  
            
@include('Elements.footer')  

        
<!-- JS -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    
    
</body>
</html>
