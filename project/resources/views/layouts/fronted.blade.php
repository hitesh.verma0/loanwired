<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <title>{{ config('app.name', 'Home') }}</title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
            <link rel="icon" href="images/favicon.png" type="image/x-icon">
            <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
            <link rel="icon" href="images/favicon.png" type="image/x-icon">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">

            <link href="{{ asset('css/style-main.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ asset('css/media-main.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ asset('css/media.css') }}" rel="stylesheet" type="text/css"> 
            <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"> 
            <link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css"> 
            <link rel="stylesheet" href="{{ asset('js/emi/simple-slider.css') }}">
            <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
            <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
        </head>
        <body>  
            {{-- @include('Elements.header') --}}
            @yield('content')  
            
            @include('Elements.footer')  
        <script type="text/javascript" src="{{ asset('js/emi/simple-slider.js') }}"></script>  
        <script type="text/javascript" src="{{ asset('js/emi/loan-elgiblity.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/emi/calculator.js') }}"></script> 
        
        <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>  
    </body> 
</html>
