<!DOCTYPE HTML>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <title>{{ config('app.name', 'Login') }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">

   
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"> 
    <link href="{{ asset('css/media.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">  
    {{-- <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
     --}}
     <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script> 
     <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script> 
     <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script> 
    </head>
<body>
        @yield('content')

<!-- JS --> 
<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/regiCustom.js') }}"></script>
<script type="text/javascript">
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script> 
<script>
        let timerOn = true;

        function timer(remaining) {
            var m = Math.floor(remaining / 60);
            var s = remaining % 60;

            m = m < 10 ? '0' + m : m;
            s = s < 10 ? '0' + s : s;
            document.getElementById('timer').innerHTML = m + ':' + s;
            remaining -= 1;

            if(remaining >= 0 && timerOn) {
                setTimeout(function() {
                    timer(remaining);
                }, 1000);
                return;
            }

            if(!timerOn) {
                // Do validate stuff here
                return;
            }

            // Do timeout stuff here
            // alert('Timeout for otp'); 
        }
        
        $('#regenerateOTP').on('click', function (e) {
            // e.preventDefault();   
                $.ajax({
                    url: "{{ route('resendotp') }}",
                    type: 'post',
                    data:{contactNo: $("#usermob").val()},
                    success: function (response) { 
                        $('#regenerateOTP').attr("disabled","disabled").css('pointer-events','none');
                        timer(60); 
                        setTimeout(function() { 
                            $('#regenerateOTP').removeAttr("disabled").css('pointer-events','auto');
                        }, 60000);  
                        var obj = jQuery.parseJSON(response); 
                        if (obj.status=="failed") {
                            message_box(obj.message, "OTP Resent","error");
                        }else{
                            message_box(obj.message,"OTP Resent","info");
                        } 
                    },
                    error: function () {
                        // swal({ title: "Error!", text: "We are facing technical error!", type: "error", timer: 2000, confirmButtonText: "Ok" });
                        return false;
                    }
                });  
        }); 
    </script>
   <?php if(isset($isTimerStart)&&!empty($isTimerStart)){ ?>
   <script>
    $(document).ready(function(){
        $('#regenerateOTP').attr("disabled","disabled").css('pointer-events','none');
        timer(60);
        setTimeout(function() { 
            $('#regenerateOTP').removeAttr("disabled").css('pointer-events','auto');
        }, 60000); 
    });
</script>
    <?php }?> 
</body>
</html>
