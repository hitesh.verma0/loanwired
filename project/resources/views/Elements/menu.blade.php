{{-- Home page menu --}}
<nav class="navbar navbar-expand-lg navbar-light"> <a class="navbar-brand" href="{{ url('/')}}">
    <img src="images/main-page/logo-img.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        {{-- <li class="nav-item active"> <a class="nav-link" href="{{ url('/products')}}">Products</a> </li> --}}
        <li class="nav-item active"> <a class="nav-link" data-scroll href="#products">Products</a> </li>
        <li class="nav-item active"> <a class="nav-link" data-scroll href="#overview">About Us</a> </li> 
        {{-- <li class="nav-item"> <a class="nav-link" href="{{ url('/contact-us')}}">Contact Us</a> </li> --}}
        <li class="nav-item active"> <a class="nav-link" data-scroll href="#footercontact">Contact Us</a> </li>
        <li class="nav-item active"> <a class="nav-link" data-scroll href="#quickapply">Apply</a> </li>
        <li class="nav-item"> <a class="nav-link" href="{{ url('/login')}}">Login</a> </li>
        {{-- <li class="nav-item"> <a class="nav-link" href="{{ url('/about-us')}}">About Us</a> </li> --}}
        
        {{-- <li class="nav-item btn-aply"> <a class="nav-link" href="{{ url('/apply')}}">Apply</a> </li> --}}
      </ul>
    </div>
</nav>
{{-- End Home  page menu --}}