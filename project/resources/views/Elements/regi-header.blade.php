<div class="side-menu-"> 
    <ul>
      @php
      $id=(isset($id))?$id:"";    
      @endphp
      <li > <a class="nav-link" target="_blank" data-scroll href="{{ url('/#products')}}">Products</a> </li>
      <li > <a class="nav-link" target="_blank" data-scroll href="{{ url('/#overview')}}">About Us</a> </li>  
      <li > <a class="nav-link" target="_blank" data-scroll href="#footercontact">Contact Us</a> </li>
      <li > <a class="nav-link" target="_blank" data-scroll href="{{ url('/#quickapply')}}">Apply</a> </li>
      <li class="nav-item"> <a target="_blank" class="nav-link" href="{{ url('/user-login')}}">Login</a> </li>

      {{-- <li><a class="{{($action=='apply')?'active':''}}" href="{{url('/loan/apply')}}">Personal Details</a></li>
      <li><a class="{{($action=='professional_details')?'active':''}}" href="{{ ((!empty($id)&&!empty($stepCompleted)&&($stepCompleted>= 2 && $stepCompleted<=5) ) )?url('/loan/professional-details/'.$id):'javascript:void(0)'}}">Professional Details</a></li>
      <li><a class="{{($action=='congratulations')?'active':''}}" href="{{ ((!empty($id)&&!empty($stepCompleted)&&($stepCompleted >= 2 && $stepCompleted<=5))) ?url('/loan/congratulations/'.$id):'javascript:void(0)'}}">Congratulations</a></li>
      <li><a class="{{($action=='banking_statement')?'active':''}}" href="{{ ((!empty($id)&&!empty($stepCompleted)&&($stepCompleted >= 3 && $stepCompleted<=5))) ?url('/loan/banking-statement/'.$id):'javascript:void(0)'}}">Upload Bank Statement</a></li>
      {{-- <li><a class="" href="{{(empty($id))?'javascript:void(0)':url('/personal-details')}}">Net bankig</a></li> --}}
      {{-- <li><a class="{{($action=='kyc_documents')?'active':''}}" href="{{((!empty($id)&&!empty($stepCompleted)&&($stepCompleted>= 4 && $stepCompleted<=5) ))?url('/loan/kyc-documents/'.$id):'javascript:void(0)'}}">KYC Documents</a></li>
      <li><a class="{{($action=='thank_you')?'active':''}}" href="{{((!empty($id)&&!empty($stepCompleted)&&($stepCompleted<=5)) )?url('/loan/thank-you/'.$id):'javascript:void(0)'}}">Thank You</a></li>  --}}
    </ul>
    </div>
    <div class="black-bg"></div>
    <header class="head-background">
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            <nav class="navbar-head"> <a href="javascript:void(0)" class="bar-icon"><i class="fa fa-bars"></i></a> <a href="{{url('/')}}">
              <img src="{{asset('/images/logo-img.png')}}"></a> <a href="javascript:void(0)" class="pers-men">@if(isset($productName))
                    {{$productName}}
              @else
                  Personal Loan
              @endif </a> </nav>
            <div class="head-line">
              <h3>Get started by creating your profile</h3>
            </div>
          </div>
        </div>
      </div>
    </header>