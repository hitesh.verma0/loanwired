<footer class="footer-sec" id="footercontact" style="
background: #000;
">
    <div class="container">
        <span class="fot-logo"><img src="{{ asset('images/main-page/white-logo.png') }}"  alt=""/></span>

<div class="footer section-space100">

    <!-- footer -->

    <div class="container ">

        <div class="row ">
 

            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 ">

                <div class="row ">

                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 ">

                        {{-- <h3 class="newsletter-title ">Signup Our Newsletter</h3> --}}

                    </div>

                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 "> 
                        <div class="newsletter-form"> 
                            <!-- Newsletter Form --> 
                            {{-- <form action="newsletter.php" method="post">

                                <div class="input-group">

                                    <input type="email" class="form-control border-0 shadow-none" id="newsletter" 

                                    name="newsletter" placeholder="Write E-Mail Address" >

                                    <span class="input-group-btn">

                                        <button class="btn btn-secondary" type="submit">Go!</button>

                                    </span>

                                </div>

                                <!-- /input-group -->

                            </form> --}} 
                        </div>

                        <!-- /.Newsletter Form -->

                    </div>

                </div>

                <!-- /.col-lg-6 -->

            </div>

        </div>

        <hr class="dark-line ">

        <div class="row" style="color: white;">

            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">

                <div class="widget-text mt40">

                    <!-- widget text -->

                    <p>At Loan Wired, we aim to make the loan process more holistic. Currently providing only personal loans and business loans, we look to expand in other critical areas in the upcoming years. Till then, enjoy the fastest and super-intuitive loan approval with Loan Wired.</p>

                    <div class="row ">

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                            <p class="address-text "><span><i class="icon-placeholder-3 icon-1x "></i> </span>B-303, Citi Point Premises Co-Op. Soc. Ltd. 3rd Floor,

J.B. Nagar,Anderi Kurla Road, Andheri (East),Mimbai-400059</p>
     <p class="call-text "><span class="phone-icon"><i class="fa fa-phone" aria-hidden="true"></i></span><a class="footertag" href="tel:1860-121-8600">1860-121-8600</a>
                             
                            </p>
                             <p class="call-text "><span class="phone-icon"><i class="fa fa-phone" aria-hidden="true"></i></span><a class="footertag" href="tel:1860-121-8600">02249648991</a></p>
                        </div>
 <!-- widget text 
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">

                            <p class="call-text "><span><i class="icon-phone-call icon-1x "></i></span><a class="footertag" href="tel:1860-121-8600">1860-121-8600</a></p>

                            <p class="call-text "><span><i class="icon-phone-call icon-1x "></i></span><a class="footertag" href="tel:1860-121-8600">+912249648991</a></p>

                        </div>
-->
                    </div>

                </div>

                <!-- /.widget text -->

            </div>

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 ">

                <div class="widget-footer mt40 ">

                    <!-- widget footer -->

                    <ul class="listnone ">

                        <li><a class="footertag" href="index">Home</a></li>

                        <li><a class="footertag" href="about-us">About Us</a></li>

                        <!--<li><a class="footertag" href="faq ">Faq</a></li>-->

                        <li><a class="footertag" href="contact-us">Contact Us</a></li>

                        

                           <!--   <li><a class="footertag" href="become-channel-partner">Become Channel Partner</a></li> -->

                    </ul>

                </div>

                <!-- /.widget footer -->

            </div>

            <!--   <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 ">

                <div class="widget-footer mt40 ">

                  

                    <ul class="listnone ">

                        <li><a class="footertag" href="personal-loan">Personal Loan</a></li>

                        <li><a class="footertag" href="business-loan ">Business Loan</a></li>

                        <li><a class="footertag" href="emi-calculator">EMI Calculator</a></li>

                        <li><a class="footertag" href="loan-eligibility">Eligibility Calculator</a></li>

                    </ul>

                </div>

               

            </div> -->

            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 ">

                <div class="widget-social mt40 ">

                       <ul class="listnone">

                            <li><a class="footertag" href="https://www.facebook.com/loanwired" target="_blank"><i class="fa fa-facebook-f mr-2"></i>Facebook</a></li>

                            <li><a class="footertag" href="https://www.instagram.com/loanwired/" target="_blank"><i class="fa fa-instagram mr-2 "></i>Instagram</a></li>

                            <li><a class="footertag" href="https://twitter.com/loanwired" target="_blank"><i class="fa fa-twitter mr-2"></i>Twitter</a></li>

                            <li><a class="footertag" href="https://www.linkedin.com/company/loanwired" target="_blank"><i class="fa fa-linkedin mr-2"></i>Linked In</a></li>
                            <li><a class="footertag" href="https://www.youtube.com/channel/UCclGBQMKYtu7PlIX32iuILw" target="_blank"><i class="mr-2 fa fa-youtube "></i>Youtube</a></li>

                        </ul>

                    </div>

            </div>

        </div>

    </div>

</div>

    <!-- /.footer -->

    <div class="tiny-footer">

        <!-- tiny footer -->

        <div class="container">

            <div class="row">

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">

                    <p class="footertag">© Copyright <?php echo date('Y'); ?> | LoanWired Finteh (PVT.) Limited</p>

                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-right">

            <p><a  class="footertag" href="Terms_of_Service.html">Terms of use</a> | <a class="footertag" href="Privacy_policy.html">Privacy Policy</a></p>

                </div>

            </div>

        </div>

    </div>

    <!-- /.tiny footer -->

    <!-- back to top icon -->

    <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->

    </div>
</footer>