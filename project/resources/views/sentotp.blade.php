@extends('layouts.login')
@section('content')
    <section class="login-section">
        <div class="login-div">
            <div class="left-logo-img">
                <i><img src="images/login-bg.jpg"  alt=""/></i>
                <span><img src="images/login-bg-side.jpg"  alt=""/></span>
            </div>  
            <div class="form-login"><div class="mid-form">
                <div class="heading-se"><h5>Welcome Back</h5>
                    <i>Get an Instant Loan</i></div>
                    <div class="login-form-sec">
                        <b>Please Verify your Mobile Number</b>
                        <form action="{{'verifyotp'}}" method="post">
                            {{ csrf_field() }}
                            <div class="mobile-no">
                                <em>OTP Number</em> 
                                <input type="hidden" id="usermob" name="mobile" value="{{$mobile}}"> 
                                <input type="hidden" id="redirect" name="redirect" value="{{$redirect}}">
                                
                                <input type="password" name="otp" class="{{ $errors->has('otp') ? 'is-invalid' : '' }}"
                                    value="{{ old('otp') }}" onkeypress="return isNumber();" placeholder="_   _   _   _" pattern="\d*" maxlength="4
                                    " required="required">
                                @if($errors->has('otp'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('otp') }}</strong>
                                    </div>
                                @endif
                            </div> 
                            <button type="submit">Verify</button>
                        </form> 
                        <p ><a id="regenerateOTP" class="pull-right" href="javascript::void(0)">Resend OTP</a> </p>
                        <div>Time left = <span id="timer">0.00</span></div>
                    </div>
                </div>
            </div> 
        </div>
    </section>  
    
@endsection
