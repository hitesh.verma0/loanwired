@extends('layouts.fronted')
@section('content')
</style>
<div class="bg-banner">
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            @include('Elements.menu')
          </div>
        </div>
      </div>
    </header>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-7" >
            <div class="contant-left" id="quickapply">
              <h1>Get Instant Loan</h1>
              <h5>Choose the Best Loan. Apply Now!</h5>
              <form action="{{'quickapply'}}" method="post" id="loginbymob">
                {{ csrf_field() }}
                <div class="form-group mobiles" id="applyp"> <span class="mobile-num">+91</span>
                    <input type="text" name="mobile" id="mobilenumber" aria-describedby="mobileHelp" class="form-control {{ $errors->has('mobile') ? 'is-invalid' : '' }}" value="{{ old('mobile') }}" onkeypress="return isNumber();" placeholder="Enter Mobile Number..." pattern="\d*" maxlength="10" required="required">
                      @if($errors->has('mobile'))
                          <div class="invalid-feedback">
                              <strong>{{ $errors->first('mobile') }}</strong>
                          </div>
                      @endif 
                      <button type="submit" id="sendotp" style="opacity: 0;"></button>
                      <a href="javascript:void(0)" onclick="submitloginbymob()" class="right-btn text-center"><img class="link wobble-horizontal-on-hover" src="images/main-page/move-arrow.png"></a>
                    </div>
                </form>
            </div>
          </div>
          <div class="col-sm-5">
            <div class="img-right"> <img src="images/home-banner.gif"> </div>
          </div>
        </div>
      </div>
    </section>
  </div>  
<section class="bg-pink" id="products">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading text-left">
          <h2>OUR Product</h2>
          <hr>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="loan-box">
          <div class="top-img"> <img src="images/main-page/loan-img-1.png"> </div>
          <div class="img-contant">
            <h3>Personal Loan</h3>
          </div>
          <div class="amo-age-doc">
            <div class="loan-amount text-center"> <i>Loan Amount</i> <em>upto 99 Lakhs</em> </div>
            <div class="loan-amount text-center"> <i>Age</i> <em>22 to 60</em> </div>
            <div class="loan-amount text-center"> <i>Interest Rate</i><em>10.5% - 16% </em></div>
            <div class="loan-amount text-center"> <i>Doc. KYC</i> <em>PAN/Aadhar</em> </div>
          </div>
          <div class="button text-right">
           <a href="#applyp" class="apply-btn">Apply</a>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="loan-box">
          <div class="top-img"> <img src="images/main-page/loan-img-1.png"> </div>
          <div class="img-contant">
            <h3>Business Loan</h3>
          </div>
          <div class="amo-age-doc">
            <div class="loan-amount text-center"> <i>Loan Amount</i> <em>upto 5 Crores</em> </div>
            <div class="loan-amount text-center"> <i>Age</i> <em>22 to 60</em> </div>
            <div class="loan-amount text-center"> <i>Interest Rate</i><em>13.5% - 22% </em></div>
            <div class="loan-amount text-center"> <i>Doc. KYC</i> <em>Business Proof</em> </div>
          </div>
          <div class="button text-right">
            <a href="#applyp" class="apply-btn">Apply</a>
          </div>
        </div>
      </div>
<!--       <div class="col-md-6">
        <div class="loan-box">
          <div class="top-img"> <img src="images/main-page/loan-img-1.png"> </div>
          <div class="img-contant">
            <h3>Personal Loan</h3>
          </div>
          <div class="amo-age-doc">
            <div class="loan-amount text-center">
              <h6>Loan Amount</h6>
              <h2>99Laks</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Age</h6>
              <h2>22 to 60</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Interest Rate</h6>
            </div>
            <div class="loan-amount text-center">
              <h6>Doc. KYC</h6>
              <h2>PAN/ Aadhar</h2>
            </div>
          </div>
          <div class="button text-right">
            <button class="apply-btn">Apply</button>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="loan-box">
          <div class="top-img"> <img src="images/main-page/loan-img-1.png"> </div>
          <div class="img-contant">
            <h3>Personal Loan</h3>
          </div>
          <div class="amo-age-doc">
            <div class="loan-amount text-center">
              <h6>Loan Amount</h6>
              <h2>99Laks</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Age</h6>
              <h2>22 to 60</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Interest Rate</h6>
            </div>
            <div class="loan-amount text-center">
              <h6>Doc. KYC</h6>
              <h2>PAN/ Aadhar</h2>
            </div>
          </div>
          <div class="button text-right">
            <button class="apply-btn">Apply</button>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="loan-box">
          <div class="top-img"> <img src="images/main-page/loan-img-1.png"> </div>
          <div class="img-contant">
            <h3>Personal Loan</h3>
          </div>
          <div class="amo-age-doc">
            <div class="loan-amount text-center">
              <h6>Loan Amount</h6>
              <h2>99Laks</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Age</h6>
              <h2>22 to 60</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Interest Rate</h6>
            </div>
            <div class="loan-amount text-center">
              <h6>Doc. KYC</h6>
              <h2>PAN/ Aadhar</h2>
            </div>
          </div>
          <div class="button text-right">
            <button class="apply-btn">Apply</button>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="loan-box">
          <div class="top-img"> <img src="images/main-page/loan-img-1.png"> </div>
          <div class="img-contant">
            <h3>Personal Loan</h3>
          </div>
          <div class="amo-age-doc">
            <div class="loan-amount text-center">
              <h6>Loan Amount</h6>
              <h2>99Laks</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Age</h6>
              <h2>22 to 60</h2>
            </div>
            <div class="loan-amount text-center">
              <h6>Interest Rate</h6>
            </div>
            <div class="loan-amount text-center">
              <h6>Doc. KYC</h6>
              <h2>PAN/ Aadhar</h2>
            </div>
          </div>
          <div class="button text-right">
            <button class="apply-btn">Apply</button>
          </div>
        </div>-->
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading text-left">
          <h2>How iT Work</h2>
          <hr>
        </div>
        <div class="slide"> <img src="images/main-page/slide-banner-img.png">
          <h2>Application Process made easy</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-4">
        <div class="first-1"> <strong>1</strong> <b>User Login</b>
          <p>Fill in the details <Br>
            Enter your basic details <Br>
            And tell us a bit about your loan requirements <Br>
            </p>
        </div>
      </div>
      <div class="col-xl-4">
        <div class="first-1"> <strong>2</strong> <b>Documents Upload</b>
          <p>Instant Approval <Br>
          At <i>Loanwired</i>, we process the loan approval<Br>
          within the hours of submission<Br>
            </p>
        </div>
      </div>
      <div class="col-xl-4">
        <div class="first-1"> <strong>3</strong> <b>Disbursement</b>
          <p>Get Money disbursed within hours<Br>
            Once your loan is approved, <Br>
            We disburse the loan amount within hours to your bank account<Br>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
     @include('Elements.emi-calculator')
  </section>
  <section id="overview" class="backg-over over-view">
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
          <div class="heading text-left">
            <h2>Overview</h2>
            <hr>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-3">
          <div class="Customers text-center">
            <div class="Customers-images"> <img src="images/main-page/Customers-img1.png"> </div>
            <h3>2500+</h3>
            <p>Happy Customers</p>
          </div>
        </div>
        <div class="col-xl-3">
          <div class="Customers text-center">
            <div class="Customers-images"> <img src="images/main-page/Customers-img2.png"> </div>
            <h3>6000+</h3>
            <p>File Processed</p>
          </div>
        </div>
        <div class="col-xl-3">
          <div class="Customers text-center">
            <div class="Customers-images"> <img src="images/main-page/Customers-img3.png"> </div>
            <h3>1500+Cr</h3>
            <p>Loan Disbursed</p>
          </div>
        </div>
        <div class="col-xl-3">
          <div class="Customers text-center">
            <div class="Customers-images"> <img src="images/main-page/Customers-img4.png"> </div>
            <h3>60 Cities</h3>
            <p>Cities Across India</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="map-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <div class="citys-maps">
            <div class="heading-contant">
              <h2>We offer personal loan in all the major cities across india</h2>
              <h3>Available in 60+ cities</h3>
            </div>
            <div class="city-lists">
              <div class="row">
                <div class="col-md-4">
                  <div class="city-colum">
                    
  
      
  
      
  
      <ul>
      <li>Andhra Pradesh</li>
      <li>    Arunachal Pradesh</li>
      <li>    Assam</li>
      <li>    Mizoram</li>
      <li>    Nagaland</li>
      <li>    Odisha</li>
      <li>    Punjab</li>
      <li>    Rajasthan</li>
      <li>    Sikkim</li>
      <li>    Tamil Nadu</li>
      <li>    Bihar</li>
      <li>    Chhattisgarh</li>
      <li>    Goa</li>
      <li>    Gujarat</li>
      <li>    Haryana</li>
      <li>    Himachal Pradesh</li>
      <li>    Jharkhand</li>
      <li>    Karnataka</li>
  </ul>
      
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="city-colum">
                    
  
      <ul>
      <li>Manipur</li>
      <li>    Meghalaya</li>
      <li>    Mizoram</li>
      <li>    Nagaland</li>
      <li>    Odisha</li>
      <li>    Punjab</li>
      <li>    Rajasthan</li>
      <li>    Sikkim</li>
      <li>    Tamil Nadu</li>
      <li>    Telangana</li>
      <li>    Tripura</li>
      <li>    Uttarakhand</li>
      <li>    Uttar Pradesh</li>
      <li>    West Bengal</li>
      <li>    Mizoram</li>
      <li>    Nagaland</li>
      <li>    Odisha</li>
      <li>    Punjab</li>
  </ul>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="city-colum">
                    <ul>
      <li>Andhra Pradesh</li>
      <li>    Arunachal Pradesh</li>
      <li>    Assam</li>
      <li>    Bihar</li>
      <li>    Chhattisgarh</li>
      <li>    Goa</li>
      <li>    Gujarat</li>
      <li>    Haryana</li>
      <li>    Himachal Pradesh</li>
      <li>    Jharkhand</li>
      <li>    Karnataka</li>
      <li>    Kerala</li>
      <li>    Madhya Pradesh</li>
      <li>    Mizoram</li>
      <li>    Nagaland</li>
      <li>    Odisha</li>
      <li>    Punjab</li>
      <li>    Rajasthan</li>
  </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="map-images"> <img src="images/main-page/indias-map-img.png"> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="backg-over">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="heading text-left vision-sec">
            <h2>Overview</h2>
            <hr>
            <p>We are a Fintech startup specializing in personal and business loans. We are an AI-powered organization to make the loaning process more intuitive and holistic for all the parties involved.</p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="our-Mission"> <img src="images/main-page/Mission-img.png"> </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
          <div class="heading text-left">
            <h2>Our Values</h2>
            <hr>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-4">
          <div class="Our-Values">
            <div class="img-Our-Values"> <img src="images/main-page/Values-img.png"> </div>
            <div class="contant-Our-Values">
              <b>Who We Are?</b>
              <p>Loan Wired is a fast-growing, lending platform helping thousands of people secure loans matching their needs.</p>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="Our-Values">
            <div class="img-Our-Values"> <img src="images/main-page/Values-img2.png"> </div>
            <div class="contant-Our-Values">
              <b>What We Offer?</b>
              <p>We are trying to provide easier access to finances for the middle and lower-class people in India.</p>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="Our-Values">
            <div class="img-Our-Values"> <img src="images/main-page/Values-img3.png"> </div>
            <div class="contant-Our-Values">
              <b>What we envisage?</b>
              <p>With the help of technology, innovation, and a team of hard-working individuals, we are looking to make complex decisions simpler for you. It would require a cohesive approach for us to reach there.</p>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
