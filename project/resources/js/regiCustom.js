$(document).ready(function(){
    $(".bar-icon").click(function(){
      $("body").addClass("show-menu");
    });
    
    $(".black-bg").click(function(){
      $("body").removeClass("show-menu");
    }); 
  }); 

function confirmAction(href, msg,title='Confirmation',status='success',showCancelButton=false) {
    var defaultMsg = 'Are you sure? you want perform this action.';
    defaultMsg = typeof msg !== 'undefined' ? msg : defaultMsg;
     swal.fire({
        title:title,
        text:defaultMsg,
        type: status,
        showCancelButton: showCancelButton,
        cancelButtonText: 'cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false,
        icon: status,
     }).then((obj) => {
        if(obj.value==true){
          location.href = href;
          return true;
      }
    });
    return false;
} 

function reasonMessage(href="") { 
         var locale = {
            OK: 'I Suppose',
            CONFIRM: 'Send',
            CANCEL: 'Cancel'
        };
        bootbox.addLocale('custom', locale);
        bootbox.prompt({ 
            inputType: 'textarea',
            title: "Please share the reason for disapproval.", 
            locale: 'custom',
            callback: function (result) {
                if(result!=null)
                   location.href = href+'?reason='+result;
                   return true;  
                // console.log('This was logged in the callback: ' + result);
            }
        });
        return false;
}
/*ajax start stop loading*/

function showLoadingIndicator() {
    html = '<div class="spinner-container"><div class="loader-main"><div class="cssload-loader"><div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div></div><p>Please wait ...</p></div></div>'; 
    $('body').append(html);
}

function hideLoadingIndicator() {
    $('.spinner-container').remove();
}

$(document).ajaxStart(function () {
    showLoadingIndicator();
});

$(document).ajaxComplete(function (event, xhr, settings) {
    hideLoadingIndicator();
});


function error_check(jqXHR, exception) {
    var msg = error_messages(jqXHR, exception);
    message_alert(msg, 'error');
}

function error_messages() {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
    }
    return msg;
}
  

function message_box(text, heading, icon="info",position="top-right"){
    $.toast({
        heading: heading,
        text: text,
        position:position,
        stack: false,
        icon: icon
    });
}

function update_fields(id,model,field,value) { 
    $.ajax({
        url: SITEURL + 'ajax/update_fields',
        type: 'POST',
        data: {
            id: id,
            model: model,
            value: value,
            field: field
        },
        //async: false,
        success: function (response) {
            var obj = jQuery.parseJSON(response);
            if ($.trim(obj.status)) {
                messgae_box(obj.message, obj.heading, obj.icon);
            }
        }
    });
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
 

function setCookie(cname, cvalue,exdays='1') {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname){
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

   
function validationrecaptcha() {
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
       return "false";
    }
    return "true";
} 
  
 

function changeStatus(id,status,model) {
     $.ajax({
        url: SITEURL+'ajax/change_status/',
        type: 'POST',
        data:{id:id,model:model,status:status},
        success: function (response) {
        if(response!=0){   
         $("#change_status_"+id).replaceWith(response);
        }else{
           alert("This is special news.please remove from special news than Inactive.");
          }
      }  
    });
}


function confirmAction(href, msg,title='Confirmation',status='success',showCancelButton=false) {
    var defaultMsg = 'Are you sure? you want perform this action.';
    defaultMsg = typeof msg !== 'undefined' ? msg : defaultMsg;
     swal.fire({
        title:title,
        text:defaultMsg,
        type: status,
        showCancelButton: showCancelButton,
        cancelButtonText: 'cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false,
        icon: status,
     }).then((obj) => {
        if(obj.value==true){
          location.href = href;
          return true;
      }
    });
    return false;
}
 
 
function loantype(selected){ 
    $(".pers-men").text($("#loantypeoption [value='"+selected+"']").text());
}


$(document).on('focus',".datepicker", function(){
       $(this).datepicker({changeMonth: true,
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: "c-100:c+100",
            maxDate:0,
            todayHighlight: true,
            autoclose: true 
        }); 
});

$(document).on('focus',".nomindatepicker", function(){
       $(this).datepicker({changeMonth: true,
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: "c-100:c+100", 
            todayHighlight: true,
            autoclose: true 
        }); 
});

$(document).on('focus',".datepickerwithfuturedate", function(){
    $(this).datepicker({changeMonth: true,
        dateFormat: 'dd/mm/yy',
        changeYear: true,
        yearRange: "c-100:c+100",
        todayHighlight: true,
        autoclose: true 
    });
}); 

function closeuploadedDocument(fieldName){
    $("#hide-"+fieldName).removeClass("hide").addClass("show");
    $("#show-"+fieldName).removeClass("show").addClass("hide");
}